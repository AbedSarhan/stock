﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GreeceStockMarketWCF.Models.StockMarketStructure
{
    [DataContract]
    public class CompanyModel : StockMarketStructureModelAbstract
    {

        #region Constructors

        public CompanyModel()
        {

        }

        public CompanyModel(int ID, string Name, IndexModel Index = null)   
        {
            this.ID = ID;
            this.Name = Name;

            this.Index = Index;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Refers to the index that this company belongs to
        /// </summary>
        [DataMember]
        public IndexModel Index 
        {
            get { return this.parent as IndexModel; }
            set { this.parent = value; }
        }

        #endregion

    }
}