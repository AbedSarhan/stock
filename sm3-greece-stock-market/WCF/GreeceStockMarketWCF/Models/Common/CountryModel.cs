﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GreeceStockMarketWCF.Models.Common
{
    [DataContract]
    public class CountryModel
    {

        #region Constructors

        public CountryModel()   
        {

        }

        public CountryModel(int ID, string Name) 
        {
            this.ID = ID;
            this.Name = Name;
        }

        #endregion

        #region Variables

        private int id;
        private string name;

        private Dictionary<CountryCategoryModel, DateTime> categories;

        #endregion

        #region Properties

        /// <summary>
        /// ID generated in our system for the country
        /// </summary>
        [DataMember]
        public int ID       
        {
            set { this.id = value; }
            get { return this.id; }
        }

        /// <summary>
        /// Name of the country
        /// </summary>
        [DataMember]
        public string Name  
        {
            set { this.name = value; }
            get { return this.name; }
        }



        /// <summary>
        /// Categories that this country belongs to and their corresponding join date
        /// </summary>
        [DataMember]
        public Dictionary<CountryCategoryModel, DateTime> Categories 
        {
            get { return this.categories; }
            set { this.categories = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the membership info about a specific category
        /// It returns the datetime when the country joined this category
        /// It also returns <out> information about the category itself
        /// If the category returned was null, then the date returned will not be valid
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <param name="Category"></param>
        public DateTime GetMembershipInfo(int CategoryID, out CountryCategoryModel Category) 
        {
            foreach (CountryCategoryModel category in Categories.Keys)
                if (category.ID == CategoryID)
                {
                    Category = category;
                    return Categories[category];
                }
            Category = null;
            return DateTime.MinValue;
        }

        /// <summary>
        /// Checks if this country belongs to <parameter>Category</parameter>
        /// </summary>
        /// <param name="CategoryID"></param>
        public bool IsMemeberOfCategory(int CategoryID) 
        {
            foreach (CountryCategoryModel category in Categories.Keys)
                if (category.ID == CategoryID)
                    return true;
            return false;
        }

        #endregion

    }
}