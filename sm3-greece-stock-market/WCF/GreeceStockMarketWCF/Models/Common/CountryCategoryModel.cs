﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GreeceStockMarketWCF.Models.Common
{
    [DataContract]
    public class CountryCategoryModel
    {

        #region Constructors

        public CountryCategoryModel()   
        {

        }
        public CountryCategoryModel(int ID, string Name, string Description) 
        {
            this.ID = ID;
            this.Name = Name;
            this.Description = Description;
        }

        #endregion

        #region Variables

        private int id;
        private string name;

        private string description;

        #endregion

        #region Properties

        /// <summary>
        /// ID generated in our system for the category
        /// </summary>
        [DataMember]
        public int ID               
        {
            get { return this.id; }
            set { this.id = value; }
        }
       
        /// <summary>
        /// Name of the category
        /// </summary>
        [DataMember]
        public string Name          
        {
            get { return this.name; }
            set { this.name = value; }
        }



        /// <summary>
        /// Description about this category and its importance
        /// </summary>
        [DataMember]
        public string Description   
        {
            get { return this.description; }
            set { this.description = value; }
        }

        #endregion

    }
}