﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GreeceStockMarketWCF.Models.News
{
   public  class StockIndexDataModel
   {
        #region Varaibles

       int snapshotID;


        DateTime snapShotDate;


        Decimal open;


        Decimal close;


        Decimal high;


        Decimal low;



        Decimal adjClose;


        Decimal volume;

       #endregion

        #region Properties

        public Decimal Volume                       
        {
            get { return volume; }
            set { volume = value; }
        }
        public Decimal AdjClose                     
        {
            get { return adjClose; }
            set { adjClose = value; }
        }
        public Decimal Low                          
        {
            get { return low; }
            set { low = value; }
        }
        public Decimal High                         
        {
            get { return high; }
            set { high = value; }
        }
        public Decimal Close                        
        {
            get { return close; }
            set { close = value; }
        }
        public Decimal Open                         
        {
            get { return open; }
            set { open = value; }
        }
        public DateTime SnapShotDate                
        {
            get { return snapShotDate; }
            set { snapShotDate = value; }
        }
        public int SnapshotID                       
        {
            get { return snapshotID; }
            set { snapshotID = value; }
        }

        #endregion

        #region Constructors
        public StockIndexDataModel(int SnapshotID, DateTime SnapshotDate, Decimal Open, Decimal Close, Decimal High, Decimal Low, Decimal AdjClose, Decimal Volume)
        {
            this.volume = Volume;
            this.snapShotDate = SnapshotDate;
            this.open = Open;
            this.close = Close;
            this.high = High;
            this.low = Low;
            this.adjClose = AdjClose;
        }
        #endregion
   }
}
