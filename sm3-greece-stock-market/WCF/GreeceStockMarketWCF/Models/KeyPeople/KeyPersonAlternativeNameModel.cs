﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GreeceStockMarketWCF.Models.KeyPeople
{
    [DataContract]
    public class KeyPersonAlternativeNameModel
    {

        #region Constructors

        public KeyPersonAlternativeNameModel()
        {

        }

        public KeyPersonAlternativeNameModel(string Name, string Notes) 
        {
            this.Name = Name;
            this.Notes = Notes;
        }

        #endregion

        #region Variables

        private string name;
        private string notes;

        #endregion

        #region Properties

        /// <summary>
        /// Alternative name
        /// </summary>
        [DataMember]
        public string Name  
        {
            get { return this.name; }
            set { this.name = value; }
        }


        /// <summary>
        /// Notes (such as the api which uses this name as an identifier)
        /// </summary>
        [DataMember]        
        public string Notes 
        {
            get { return this.notes; }
            set { this.notes = value; }
        }

        #endregion

    }
}