﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GreeceStockMarketWCF.Models.KeyPeople
{
    [DataContract]
    public class KeyPersonWebInfoModel
    {

        #region Constructors

        public KeyPersonWebInfoModel()  
        {

        }
        public KeyPersonWebInfoModel(string Key, string Value)  
        {
            this.Key = Key;
            this.Value = Value;
            this.Children = new List<KeyPersonWebInfoModel>();
        }

        #endregion

        #region Variables

        string key;
        string value;
        List<KeyPersonWebInfoModel> children;

        #endregion

        #region Properties

        /// <summary>
        /// This is the name of an information key about the key person
        /// </summary>
        [DataMember]        
        public string Key   
        {
            get { return this.key; }
            set { this.key = value; }
        }

        /// <summary>
        /// This is the value of an information key about the key person
        /// </summary>
        [DataMember]      
        public string Value 
        {
            get { return this.value; }
            set { this.value = value; }
        }

        /// <summary>
        /// These are the children of this information piece
        /// </summary>
        [DataMember]
        public List<KeyPersonWebInfoModel> Children 
        {
            get { return this.children; }
            set { this.children = value; }
        }

        #endregion

        #region Methods

        public KeyPersonWebInfoModel AddChild(string Key, string Value)  
        {
            KeyPersonWebInfoModel item = new KeyPersonWebInfoModel(Key, Value);
            Children.Add(item);
            return item;
        }

        public void AddChild(KeyPersonWebInfoModel KeyPersonWebInfoModel)
        {
            children.Add(KeyPersonWebInfoModel);
        }

        #endregion

    }
}