﻿using GreeceStockMarketWCF.Controllers.Common.DatabaseControllers;
using GreeceStockMarketWCF.Models.Common;
using GreeceStockMarketWCF.Models.StockMarketStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreeceStockMarketWCF.Controllers.StockMarketStructureControllers.DatabaseControllers
{
    public class StockMarketStructureDatabaseController
    {

        #region Common Methods

        private int AddStockMarketStructureObject(string Name, int ParentID, StockMarketStructureModelType Type, out string Message, MSSql2014Controller DatabaseController = null) 
        {
            try
            {
                //-------------------------------------------------
                MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
                if (DatabaseController == null && dbController.OpenConnection(out Message)) return -1;
                //-------------------------------------------------
                int GeneratedID = -1;

                //Write your content here
                // ...

                //-------------------------------------------------
                if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
                //-------------------------------------------------
                Message = "Added successfully";
                return GeneratedID;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return -1;
            }
        }

        private StockMarketStructureModelAbstract GetStockMarketStructureObject(int ID, out string Message, bool WithSourceIDs = false, 
            MSSql2014Controller DatabaseController = null)                                                                                      
        {
            try
            {
                //-------------------------------------------------
                MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
                if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
                //-------------------------------------------------
                StockMarketStructureModelAbstract obj = null;

                int TypeID = -1;
                string Name = null;
                int ParentID = -1;

                SerializableDictionary<string, string> SourceIDs = null;

                //Write your content here
                // ...
                

                if (WithSourceIDs)
                {
                    SourceIDs = GetStockMarketStructureObjectSourceIDs(ID, out Message, dbController);
                    if (SourceIDs == null)
                        return null;
                }

                switch (TypeID)
                {
                    case (int)StockMarketStructureModelType.Market:
                        obj = new MarketModel(ID, Name);
                        obj.SourceIDs = SourceIDs;
                        break;
                    case (int)StockMarketStructureModelType.Index:
                        obj = new IndexModel(ID, Name);
                        obj.SourceIDs = SourceIDs;
                        ((IndexModel)obj).Market = new MarketModel(ParentID, "N/A");
                        break;
                    case (int)StockMarketStructureModelType.Company:
                        obj = new CompanyModel(ID, Name);
                        obj.SourceIDs = SourceIDs;
                        ((CompanyModel)obj).Index = new IndexModel(ParentID, "N/A");
                        break;
                    default:
                        break;
                }

                //-------------------------------------------------
                if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
                //-------------------------------------------------
                Message = obj == null ? "Unrecognized type" : "Fetched successfully";
                return obj;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        private SerializableDictionary<string, string> GetStockMarketStructureObjectSourceIDs(int ID, out string Message, 
            MSSql2014Controller DatabaseController = null)                                                                                      
        {
            try
            {
                //-------------------------------------------------
                MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
                if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
                //-------------------------------------------------

                SerializableDictionary<string, string> SourceIDs = null;

                //Write your content here
                // ...


                //-------------------------------------------------
                if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
                //-------------------------------------------------
                Message = "Fetched successfully";
                return SourceIDs;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        private List<StockMarketStructureModelAbstract> GetStockMarketStructureObjectChildren(int ID, out string Message, 
            MSSql2014Controller DatabaseController = null)                                                                                      
        {
            try
            {
                //-------------------------------------------------
                MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
                if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
                //-------------------------------------------------

                List<StockMarketStructureModelAbstract> Children = null;

                //Write your content here
                // ...


                //-------------------------------------------------
                if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
                //-------------------------------------------------
                Message = "Fetched successfully";
                return Children;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        private List<StockMarketStructureModelAbstract> GetListOfStockMarketStructureObjectsByType(out string Message, bool WithSourceIDs = false,
            bool WithMarkets = false, bool WithIndexes = false, bool WithCompanies = false, MSSql2014Controller DatabaseController = null)      
        {
            try
            {
                //-------------------------------------------------
                MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
                if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
                //-------------------------------------------------
                List<StockMarketStructureModelAbstract> clusteredObjects = new List<StockMarketStructureModelAbstract>();
                Dictionary<int, StockMarketStructureModelAbstract> allObjects = new Dictionary<int, StockMarketStructureModelAbstract>();

                //Write your content here
                // ...


                //-------------------------------------------------
                if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
                //-------------------------------------------------

                foreach (int ID in allObjects.Keys)
                {
                    StockMarketStructureModelAbstract obj = allObjects[ID];
                    if (obj is MarketModel)
                    {
                        clusteredObjects.Add(obj);
                    }
                    else if (obj is IndexModel)
                    {
                        int ParentID = ((IndexModel)obj).Market.ID;
                        if (allObjects.ContainsKey(ParentID))
                            ((MarketModel)allObjects[ParentID]).AddIndex(obj as IndexModel);
                        else
                            clusteredObjects.Add(obj);
                    }
                    else if (obj is CompanyModel)
                    {
                        int ParentID = ((CompanyModel)obj).Index.ID;
                        if (allObjects.ContainsKey(ParentID))
                            ((IndexModel)allObjects[ParentID]).AddCompany(obj as CompanyModel);
                        else
                            clusteredObjects.Add(obj);
                    }
                }

                Message = "Fetched successfully";
                return clusteredObjects;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        #endregion


        #region Market Methods

        public MarketModel AddMarket(string Name, out string Message, MSSql2014Controller DatabaseController = null)    
        {
            int GeneratedID = AddStockMarketStructureObject(Name, -1, StockMarketStructureModelType.Market, out Message, DatabaseController);
            return GeneratedID == -1 ? null : new MarketModel(GeneratedID, Name);
        }

        public MarketModel GetMarket(int ID, out string Message, bool WithSourceIDs = false, 
            bool WithIndexes = false, bool WithCompanies = false, MSSql2014Controller DatabaseController = null)        
        {
            //-------------------------------------------------
            MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
            if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
            //-------------------------------------------------

            MarketModel market = GetStockMarketStructureObject(ID, out Message, WithSourceIDs, dbController) as MarketModel;
            if (market == null) return null;

            if (WithIndexes)
            {
                List<StockMarketStructureModelAbstract> children = GetStockMarketStructureObjectChildren(ID, out Message, dbController);
                if (children == null)
                    return null;
                market.AddIndexes(children);
                if (WithCompanies)
                {
                    foreach (IndexModel child in market.Indexes.Values)
                    {
                        List<StockMarketStructureModelAbstract> subChildren = GetStockMarketStructureObjectChildren(child.ID, out Message, dbController);
                        if (subChildren == null)
                            return null;
                        child.AddCompanies(subChildren);
                    }
                }
            }

            //-------------------------------------------------
            if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
            //-------------------------------------------------
            return market;
        }

        public List<MarketModel> GetListOfAllMarkets(out string Message, bool WithSourceIDs = false,
            bool WithIndexes = false, bool WithCompanies = false, MSSql2014Controller DatabaseController = null)        
        {
            List<StockMarketStructureModelAbstract> objects = GetListOfStockMarketStructureObjectsByType(out Message, WithSourceIDs, true, WithIndexes, WithCompanies, DatabaseController);
            if (objects == null) return null;
            List<MarketModel> markets = new List<MarketModel>();
            foreach (StockMarketStructureModelAbstract obj in objects)
                if (obj as MarketModel == null)
                    continue;
                else
                    markets.Add(obj as MarketModel);
            return markets;
        }

        #endregion

        #region Index Methods

        public IndexModel AddIndex(string Name, int MarketID, out string Message, MSSql2014Controller DatabaseController = null)    
        {
            int GeneratedID = AddStockMarketStructureObject(Name, MarketID, StockMarketStructureModelType.Index, out Message, DatabaseController);
            return GeneratedID == -1 ? null : new IndexModel(GeneratedID, Name);
        }

        public IndexModel GetIndex(int ID, out string Message, bool WithSourceIDs = false,
            bool WithCompanies = false, MSSql2014Controller DatabaseController = null)                                              
        {
            //-------------------------------------------------
            MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
            if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
            //-------------------------------------------------

            IndexModel index = GetStockMarketStructureObject(ID, out Message, WithSourceIDs, dbController) as IndexModel;
            if (index == null) return null;

            MarketModel parent = GetStockMarketStructureObject(index.Market.ID, out Message, WithSourceIDs, dbController) as MarketModel;
            if (parent == null) return null;

            index.Market = parent;

            if (WithCompanies)
            {
                List<StockMarketStructureModelAbstract> children = GetStockMarketStructureObjectChildren(ID, out Message, dbController);
                if (children == null)
                    return null;
                index.AddCompanies(children);
            }

            //-------------------------------------------------
            if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
            //-------------------------------------------------
            return index;
        }

        public List<IndexModel> GetListOfAllIndexes(out string Message, bool WithSourceIDs = false,
            bool WithCompanies = false, MSSql2014Controller DatabaseController = null)                                              
        {
            List<StockMarketStructureModelAbstract> objects = GetListOfStockMarketStructureObjectsByType(out Message, WithSourceIDs, false, true, WithCompanies, DatabaseController);
            if (objects == null) return null;
            List<IndexModel> indexes = new List<IndexModel>();
            foreach (StockMarketStructureModelAbstract obj in objects)
                if (obj as IndexModel == null)
                    continue;
                else
                    indexes.Add(obj as IndexModel);
            return indexes;
        }

        #endregion

        #region Company Methods

        public CompanyModel AddCompany(string Name, int IndexID, out string Message, MSSql2014Controller DatabaseController = null) 
        {
            int GeneratedID = AddStockMarketStructureObject(Name, IndexID, StockMarketStructureModelType.Company, out Message, DatabaseController);
            return GeneratedID == -1 ? null : new CompanyModel(GeneratedID, Name);
        }

        public CompanyModel GetCompany(int ID, out string Message, bool WithSourceIDs = false, MSSql2014Controller DatabaseController = null)   
        {
            //-------------------------------------------------
            MSSql2014Controller dbController = DatabaseController == null ? new MSSql2014Controller() : DatabaseController;
            if (DatabaseController == null && dbController.OpenConnection(out Message)) return null;
            //-------------------------------------------------

            CompanyModel company = GetStockMarketStructureObject(ID, out Message, WithSourceIDs, dbController) as CompanyModel;
            if (company == null) return null;

            IndexModel parent = GetStockMarketStructureObject(company.Index.ID, out Message, WithSourceIDs, dbController) as IndexModel;
            if (parent == null) return null;

            company.Index = parent;

            //-------------------------------------------------
            if (DatabaseController == null && !dbController.CloseConnection(out Message)) ;
            //-------------------------------------------------
            return company;
        }

        public List<CompanyModel> GetListOfAllCompanies(out string Message, bool WithSourceIDs = false, MSSql2014Controller DatabaseController = null)  
        {
            List<StockMarketStructureModelAbstract> objects = GetListOfStockMarketStructureObjectsByType(out Message, WithSourceIDs, false, false, true, DatabaseController);
            if (objects == null) return null;
            List<CompanyModel> companies = new List<CompanyModel>();
            foreach (StockMarketStructureModelAbstract obj in objects)
                if (obj as IndexModel == null)
                    continue;
                else
                    companies.Add(obj as CompanyModel);
            return companies;
        }

        #endregion

    }
}