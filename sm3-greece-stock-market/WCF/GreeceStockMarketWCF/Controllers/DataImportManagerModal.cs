﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using HtmlAgilityPack;
using System.Data;
using System.Globalization;
using System.Collections;
using GreeceStockMarketWCF.Models.News;


namespace GreeceStockMarketWCF.Models.Controllers
{
    public class DataImportController
    {

        #region Update companies
        public bool ImportCompanies(string FileName, int index)
        {


            StreamReader rules = new StreamReader(FileName);

            string line = "";
            while ((line = rules.ReadLine()) != null)
            {
                string[] val = line.Split(',');
                val[0] = val[0].Replace('(', ',');
                val[0] = val[0].Replace(')', ' ');
                string[] val2 = val[0].Split(',');

                DatabaseController.AddIndexCompany(index, val2[0].Trim(), val2[1].Trim());
            }
            return true;
        }

        #endregion



        #region Update stocks from csv  file
        public bool ImportIndexStockDataFromFile(string FileName, int index)
        {
            try
            {
                StreamReader rules = new StreamReader(FileName);

                string line = "";
                rules.ReadLine();
                while ((line = rules.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    if (!string.IsNullOrEmpty(val[0].ToString()))
                    {

                        DateTime date = DateTime.Parse(val[0].ToString());
                        decimal open = decimal.Parse(val[1].ToString());
                        decimal high = decimal.Parse(val[2].ToString());
                        decimal low = decimal.Parse(val[3].ToString());
                        decimal close = decimal.Parse(val[4].ToString());
                        decimal vol = decimal.Parse(val[5].ToString());
                        decimal adjvol = decimal.Parse(val[6].ToString());
                        DatabaseController.AddIndexSnapshot(date, index, open, close, high, low, vol, adjvol);
                    }
                }
                rules.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ImportCOmpanyStockDataFromFile(string FileName, int companyID)
        {
            try
            {
                StreamReader rules = new StreamReader(FileName);

                string line = "";
                rules.ReadLine();
                while ((line = rules.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    DateTime date = DateTime.Parse(val[0].ToString());
                    decimal open = decimal.Parse(val[1].ToString());
                    decimal high = decimal.Parse(val[2].ToString());
                    decimal low = decimal.Parse(val[3].ToString());
                    decimal close = decimal.Parse(val[4].ToString());
                    decimal vol = decimal.Parse(val[5].ToString());
                    decimal adjvol = decimal.Parse(val[6].ToString());
                    DatabaseController.AddCompanySnapshot(date, companyID, open, close, high, low, vol, adjvol);
                }
                rules.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion


        #region Update stock data Online
        public bool UpdateIndexStockDataOnline(string indexSymbol, int indexID)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + indexSymbol);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string line = "";
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                if (sr != null)
                {
                    sr.ReadLine();
                    while ((line = sr.ReadLine()) != null)
                    {
                        List<string> val = line.Split(',').ToList<string>();
                        DateTime date = DateTime.Parse(val[0].ToString());
                        decimal open = decimal.Parse(val[1].ToString());
                        decimal high = decimal.Parse(val[2].ToString());
                        decimal low = decimal.Parse(val[3].ToString());
                        decimal close = decimal.Parse(val[4].ToString());
                        decimal vol = decimal.Parse(val[5].ToString());
                        decimal adjvol = decimal.Parse(val[6].ToString());
                        DatabaseController.AddIndexSnapshot(date, indexID, open, close, high, low, vol, adjvol);
                    }

                    return true;
                }
                else
                    return false;

            }
            catch (Exception er)
            {
                return false;
            }
        }
        public bool UpdateCompanyStockMarketDataOnline(int companyID, string companySymbol)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + companySymbol);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            string line = "";
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            sr.ReadLine();
            while ((line = sr.ReadLine()) != null)
            {
                List<string> val = line.Split(',').ToList<string>();
                DateTime date = DateTime.Parse(val[0].ToString());
                decimal open = decimal.Parse(val[1].ToString());
                decimal high = decimal.Parse(val[2].ToString());
                decimal low = decimal.Parse(val[3].ToString());
                decimal close = decimal.Parse(val[4].ToString());
                decimal vol = decimal.Parse(val[5].ToString());
                decimal adjvol = decimal.Parse(val[6].ToString());
                DatabaseController.AddCompanySnapshot(date, companyID, open, close, high, low, vol, adjvol);
            }

            return true;
        }






        public bool UpdateAllCompanyStockMarketDataOnline()
        {
            List<StockMarketCompanyModel> smcs = DatabaseController.GetComapniesIndices();
            foreach (StockMarketCompanyModel smi in smcs)
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + smi.IndexSymbol);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string line = "";
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                sr.ReadLine();
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    DateTime date = DateTime.Parse(val[0].ToString());
                    decimal open = decimal.Parse(val[1].ToString());
                    decimal high = decimal.Parse(val[2].ToString());
                    decimal low = decimal.Parse(val[3].ToString());
                    decimal close = decimal.Parse(val[4].ToString());
                    decimal vol = decimal.Parse(val[5].ToString());
                    decimal adjvol = decimal.Parse(val[6].ToString());
                    DatabaseController.AddCompanySnapshot(date, smi.CompanyID, open, close, high, low, vol, adjvol);
                }
            }

            return true;
        }

        public bool UpdateAllStockMarketDataOnline()
        {
            List<StockMarketIndexModel> smis = DatabaseController.GetStockMarketsIndices();
            foreach (StockMarketIndexModel smi in smis)
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + smi.Symbol);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string line = "";
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                sr.ReadLine();
                while ((line = sr.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    DateTime date = DateTime.Parse(val[0].ToString());
                    decimal open = decimal.Parse(val[1].ToString());
                    decimal high = decimal.Parse(val[2].ToString());
                    decimal low = decimal.Parse(val[3].ToString());
                    decimal close = decimal.Parse(val[4].ToString());
                    decimal vol = decimal.Parse(val[5].ToString());
                    decimal adjvol = decimal.Parse(val[6].ToString());
                    DatabaseController.AddIndexSnapshot(date, smi.StockMarketIndexID, open, close, high, low, vol, adjvol);
                }
            }

            return true;
        }
        #endregion


        #region Fetch Website COntent
        private string FetchWebsiteContent(string URL)
        {
            try
            {
                return new System.Net.WebClient().DownloadString(URL);
            }
            catch
            {
                return "";
            }
        }

        #endregion


        #region Get Reuters and Guardain NewsModel
        public List<NewsModel> getGuardainData(DateTime fdate, DateTime tdate)
        {
            List<NewsModel> ImportantNews = new List<NewsModel>();
            string fdateparsed = "" + fdate.Year.ToString() + "-" + fdate.Month.ToString() + "-" + fdate.Day.ToString();
            string tdateparsed = "" + tdate.Year.ToString() + "-" + tdate.Month.ToString() + "-" + tdate.Day.ToString();
            string url = @"http://content.guardianapis.com/search?q=World%20news&api-key=erst3x6w98kw9vfnk48tnxdr&page-size=200&from-date=2014-01-01&to-date=2014-01-01";

            string html = FetchWebsiteContent(url);
            JObject websiteResponse = JObject.Parse(html);
            JObject response = (JObject)websiteResponse["response"];
            JValue validLogin = (JValue)response["status"].ToString();
            if (validLogin.Value.ToString().ToLower() == "ok")
            {
                JArray res = (JArray)response["results"];
                foreach (JObject obj in res)
                {
                    if (obj["sectionName"].ToString().ToLower().Contains("new") || obj["sectionName"].ToString().ToLower().Contains("politics") || obj["sectionName"].ToString().ToLower().Contains("technology"))
                    {
                        ImportantNews.Add(new NewsModel(fdate, obj["webTitle"].ToString(), obj["webUrl"].ToString(), ""));

                    }

                }
                GetGuardainArticleText(ImportantNews);
                return ImportantNews;
            }
            else
                return null;

        }
        public List<NewsModel> GetReutorsNews(DateTime fdate, DateTime tdate)
        {
            List<NewsModel> ImportantNews = new List<NewsModel>();
            string fdateparsed = String.Format("{0:D2}{1:D2}{2:D2}", fdate.Month, fdate.Day, fdate.Year);
            string tdateparsed = String.Format("{0:D2}{1:D2}{2:D2}", fdate.Month, fdate.Day, fdate.Year);

            string url = @"http://www.reuters.com/news/archive/worldNews?date=03242014";
            HtmlDocument htmlDocument = new HtmlDocument();
            string s = FetchWebsiteContent(url);

            htmlDocument.LoadHtml(s);
            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//div[@class='feature']"))
            {
                var newsTitle = node.Descendants("a").Select(n => n.InnerText).ToList();
                var newsLink = node.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                if (newsTitle.Count > 0 && newsLink != "")
                {
                    NewsModel news;
                    if (!newsLink.Contains("reuters.com"))
                        news = new NewsModel(tdate, newsTitle[0].ToString(), @"http://www.reuters.com/" + newsLink.ToString(), "");
                    else
                        news = new NewsModel(tdate, newsTitle[0].ToString(), newsLink.ToString(), "");
                    ImportantNews.Add(news);
                }
            }
            GetReutorsArticleText(ImportantNews);
            return ImportantNews;


        }

        public List<NewsModel> GetGuardainArticleText(List<NewsModel> GuardainNews)
        {
            if (GuardainNews.Count > 0)
            {
                for (int i = 0; i < GuardainNews.Count(); i++)
                {
                    NewsModel n = GuardainNews[i];
                    HtmlDocument htmlDocument = new HtmlDocument();
                    string s = FetchWebsiteContent(n.NewsUrl);
                    htmlDocument.LoadHtml(s);
                    if (s != "")
                    {


                        var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='content__article-body from-content-api js-article__body']");
                        if (spanNode != null)
                        {
                            string article = "";
                            htmlDocument.LoadHtml(spanNode.InnerHtml);
                            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                            {
                                article = article + node.InnerText;
                            }
                            GuardainNews[i].NewsArticle = article;


                        }
                    }
                    else
                        GuardainNews[i].NewsArticle = "Article was moved by Guardain";

                }
                return GuardainNews;
            }
            else
                return null;
        }



        public List<NewsModel> GetReutorsArticleText(List<NewsModel> ReutorsNews)
        {
            if (ReutorsNews.Count > 0)
            {
                for (int i = 0; i < ReutorsNews.Count(); i++)
                {
                    NewsModel n = ReutorsNews[i];
                    HtmlDocument htmlDocument = new HtmlDocument();
                    string s = FetchWebsiteContent(n.NewsUrl);
                    htmlDocument.LoadHtml(s);
                    if (s != "")
                    {


                        var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//span[@id='articleText']");
                        if (spanNode != null)
                        {
                            string article = "";
                            htmlDocument.LoadHtml(spanNode.InnerHtml);
                            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                            {
                                article = article + node.InnerText;
                            }
                            ReutorsNews[i].NewsArticle = article;


                        }
                    }
                    else
                        ReutorsNews[i].NewsArticle = "Article was moved by Reutors";

                }
                return ReutorsNews;
            }
            else
                return null;
        }

        #endregion


        #region Import symbols

        public bool ImportSymbols(string fileName)
        {
            //UpdateSymbol

            try
            {
                StreamReader rules = new StreamReader(fileName);

                string line = "";
                rules.ReadLine();
                while ((line = rules.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    DatabaseController.AddCompanySymbol(val[0].Trim(), val[1].Trim());
                    //DatabaseController.AddCompanySymbol(val[3].Trim(), val[4].Trim());
                }
                rules.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        #endregion

        #region GetDataFOrMissingSymbols
        public bool GetCompaniesWithNoData()
        {
            Dictionary<int, string> notImported = new Dictionary<int, string>();
            DataTable dt = DatabaseController.GetCompaniesWithNoData();
            if (dt == null)
                return false;
            foreach (DataRow dr in dt.Rows)
            {
                int companyID = int.Parse(dr["CompanyID"].ToString());
                String symbol = dr["indexSymbol"].ToString();
                bool imported = UpdateCompanyStockMarketDataOnline(companyID, symbol);
                if (!imported)
                {
                    notImported.Add(companyID, dr["companyName"].ToString());

                }

            }
            return true;
        }

        #endregion

        #region Calculate Important Dates weekly  First Module

        public void ImportnatDate(int indexID)
        {
            Dictionary<int, ArrayList> weekFinalgroups = new Dictionary<int, ArrayList>();
            ArrayList ImportantDates = new ArrayList();

            #region group snapshot into week groups
            List<StockIndexDataModel> sid = DatabaseController.GetStockIndexData(indexID);
            var weekGroups = sid
            .Select(p => new
            {
                stockInfo = p,
                Year = p.SnapShotDate.Year,
                Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear
                              (p.SnapShotDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
            })
            .GroupBy(x => new { x.Year, x.Week })
            .Select((g, i) => new
            {
                WeekGroup = g,
                WeekNum = i + 1,
                Year = g.Key.Year,
                CalendarWeek = g.Key.Week
            });
            #endregion

            #region Read data grom the ggrouped result and then parse the date
            foreach (var projGroup in weekGroups)
            {
                Console.WriteLine("Week " + projGroup.WeekNum);
                foreach (var proj in projGroup.WeekGroup)
                {
                    if (!weekFinalgroups.ContainsKey(projGroup.WeekNum))
                    {
                        ArrayList ar = new ArrayList();
                        ar.Add(new StockIndexDataModel(proj.stockInfo.SnapshotID, proj.stockInfo.SnapShotDate, proj.stockInfo.Open, proj.stockInfo.Close, proj.stockInfo.High, proj.stockInfo.Low, proj.stockInfo.AdjClose, proj.stockInfo.Volume));
                        weekFinalgroups.Add(projGroup.WeekNum, ar);
                    }
                    else
                    {
                        weekFinalgroups[projGroup.WeekNum].Add(new StockIndexDataModel(proj.stockInfo.SnapshotID, proj.stockInfo.SnapShotDate, proj.stockInfo.Open, proj.stockInfo.Close, proj.stockInfo.High, proj.stockInfo.Low, proj.stockInfo.AdjClose, proj.stockInfo.Volume));
                    }
                }



            }
            #endregion
            #region COMpare the start open of the week and the closeing at the end ot the week for the stok if it difference in -2 the dates of this week as important
            foreach (int key in weekFinalgroups.Keys)
            {
                ArrayList stocks = weekFinalgroups[key];
                StockIndexDataModel firstWeekDay = (StockIndexDataModel)stocks[0];
                StockIndexDataModel LastWeekDay = (StockIndexDataModel)stocks[stocks.Count - 1];

                if (Math.Abs(firstWeekDay.Open - LastWeekDay.Close) >= 500)
                {
                    ImportantDates.AddRange(stocks);
                }

            }


            #endregion
        }


        #endregion

        #region Calculate Important Dates weekly  Second and third Module

        public void ImportnatDate2(int indexID)
        {
            Dictionary<int, ArrayList> weekFinalgroups = new Dictionary<int, ArrayList>();
            ArrayList ImportantDates = new ArrayList();

            #region group snapshot into week groups
            List<StockIndexDataModel> sid = DatabaseController.GetStockIndexData(indexID);
            for (int i = 0; i < sid.Count(); i++)
            {
                if (i < sid.Count() - 1)
                {
                    if (Math.Abs(sid[i].Open - sid[i].Close) > 300 || Math.Abs(sid[i].Open - sid[i + 1].Open) > 200)
                    {
                        if (!ImportantDates.Contains(sid[i]))
                            ImportantDates.Add(sid[i]);
                        if (!ImportantDates.Contains(sid[i + 1]))
                            ImportantDates.Add(sid[i + 1]);

                    }
                }
            }

            #endregion

        }


        #endregion
    }

}

