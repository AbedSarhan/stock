﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace GreeceStockMarketWCF.Controllers.Common.FileControllers
{
    public static class FileController
    {

        /// <summary>
        /// Save any object in the specified location in XML format
        /// </summary>
        /// <param name="Path">Destination folder + file name</param>
        /// <param name="ObjectToBeSaved"></param>
        /// <param name="Types">The different associated/included types contained by the ObjectToBeSaved</param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static bool SaveFileInXMLFormat(string Path, object ObjectToBeSaved, Type[] Types, out string Message)   
        {
            try
            {
                FileStream file = new FileStream(Path, FileMode.Create);
                XmlSerializer serializer = new XmlSerializer(ObjectToBeSaved.GetType(), Types);
                serializer.Serialize(file, ObjectToBeSaved);
                file.Close();

                Message = "Saved successfully";
                return true;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return false;
            }
        }

        /// <summary>
        /// Loads an object from a specified xml file
        /// </summary>
        /// <param name="Path">Destination folder + file name</param>
        /// <param name="MainType">The expected output type</param>
        /// <param name="Types">The different associated/included types contained by the ObjectToBeSaved</param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static object LoadFileInXMLFormat(string Path, Type MainType, Type[] Types, out string Message)          
        {
            try
            {
                FileStream file = new FileStream(Path, FileMode.Open);
                XmlSerializer serializer = new XmlSerializer(MainType, Types);
                object loadedObject = serializer.Deserialize(file);
                file.Close();

                Message = "Loaded successfully";
                return loadedObject;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

    }
}