﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GreeceStockMarketWCF.Controllers.Common.WebControllers
{
    public class WebController
    {
        /// <summary>
        /// This method takes in a dictionary of parameters and their values 
        /// that are required to be formated in the right structure and encoded
        /// to avoid any error with special case letters
        /// </summary>
        /// <param name="Parameters"></param>
        /// <returns>valid structure of the parameters</returns>
        protected string BuildParameterString(Dictionary<string, string> Parameters, out string Message)                        
        {
            if (Parameters == null)
            {
                Message = "Null parameters passed in";
                return "";
            }

            try
            {
                StringBuilder parametersString = new StringBuilder();

                parametersString.Append("?");
                foreach (string key in Parameters.Keys)
                {
                    parametersString.Append(Uri.EscapeDataString(key));
                    parametersString.Append("=");
                    parametersString.Append(Uri.EscapeDataString(Parameters[key]));

                    if (key != Parameters.Keys.Last())
                        parametersString.Append("&");
                }

                Message = "Parameters formatted successfully";
                return parametersString.ToString();
            }
            catch(Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        /// <summary>
        /// This method fetches the web content based on given URL and parameters using the POST approach
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Parameters"></param>
        /// <param name="Message"></param>
        /// <returns>The website content</returns>
        public virtual string FetchWebsiteContentByPost(string URL, Dictionary<string, string> Parameters, 
            out string Message, string ContentType = "application/x-www-form-urlencoded")                                       
        {
            // Building the parameter string and checking for any error
            string parameters = BuildParameterString(Parameters, out Message);
            if (parameters == null)
                return null;

            // From http://www.hanselman.com/blog/HTTPPOSTsAndHTTPGETsWithWebClientAndCAndFakingAPostBack.aspx
            try
            {
                string url = URL + parameters;
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);

                req.ContentType = ContentType;
                req.Method = "POST";
                System.Net.WebResponse resp = req.GetResponse();
                if (resp == null) return null;
                System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch(Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }   

        /// <summary>
        /// This method fetches the web content based on given URL and parameters using the GET approach
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Parameters"></param>
        /// <param name="Message"></param>
        /// <returns>The website content</returns>
        public virtual string FetchWebsiteContentByGet(string URL, Dictionary<string, string> Parameters, out string Message)   
        {
            // Building the parameter string and checking for any error
            string parameters = BuildParameterString(Parameters, out Message);
            if (parameters == null)
                return null;

            try
            {
                string url = URL + parameters;
                return new System.Net.WebClient().DownloadString(url);
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }
    }
}