﻿using GreeceStockMarketWCF.Models.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
namespace GreeceStockMarketWCF.Controllers.News.WebControllers
{
    public class NewsController
    {


        #region Fetch Website COntent

        /// <summary>
        /// This for fetching the conetnt of a website
        /// </summary>
        private string FetchWebsiteContent(string URL)
        {
            try
            {
                return new System.Net.WebClient().DownloadString(URL);
            }
            catch
            {
                return "";
            }
        }

        #endregion


        #region Get Reuters and Guardain News

        /// <summary>
        /// This method is responisble for getting news from the Guardain APi based on start date and end date.It then fetch the articles for each of the news by calling another method
        /// The ID of thew nes now is -1
        /// It returns a list of news  with their articles,URL, and title.
        /// </summary>
        public List<NewsModel> getGuardainData(DateTime startDate, DateTime endDate)
        {
            List<NewsModel> ImportantNews = new List<NewsModel>();
            string startDateParsed = "" + startDate.Year.ToString() + "-" + startDate.Month.ToString() + "-" + startDate.Day.ToString();
            string endDateParsed = "" + endDate.Year.ToString() + "-" + endDate.Month.ToString() + "-" + endDate.Day.ToString();
            string url = @"http://content.guardianapis.com/search?q=World%20news&api-key=erst3x6w98kw9vfnk48tnxdr&page-size=200&from-date=" + startDateParsed + "&to-date=" + endDateParsed;

            string html = FetchWebsiteContent(url);
            JObject websiteResponse = JObject.Parse(html);
            JObject response = (JObject)websiteResponse["response"];
            JValue validLogin = (JValue)response["status"].ToString();
            if (validLogin.Value.ToString().ToLower() == "ok")
            {
                JArray res = (JArray)response["results"];
                foreach (JObject obj in res)
                {
                    if (obj["sectionName"].ToString().ToLower().Contains("new") || obj["sectionName"].ToString().ToLower().Contains("politics") || obj["sectionName"].ToString().ToLower().Contains("technology"))
                    {
                        string article = GetGuardainArticleText(obj["webUrl"].ToString());
                        ImportantNews.Add(new NewsModel(-1, obj["webTitle"].ToString(), obj["webUrl"].ToString(), startDate, endDate,article));

                    }

                }
                return ImportantNews;
            }
            else
                return null;

        }

        /// <summary>
        /// This method is responisble for getting Tops news  that hapened at a spefic date from the Reutors  .It then fetch the articles for each of the news by calling another method
        ///  The ID of thew nes now is -1
        /// It returns a list of news  with their articles,URL, and title.
        /// 
        /// </summary>
        public List<NewsModel> GetReutorsNews(DateTime DayDate)
        {
            List<NewsModel> ImportantNews = new List<NewsModel>();
            string dayDateParsed = String.Format("{0:D2}{1:D2}{2:D2}", DayDate.Month, DayDate.Day, DayDate.Year);


            string url = @"http://www.reuters.com/news/archive/worldNews?date=" + dayDateParsed;
            HtmlDocument htmlDocument = new HtmlDocument();
            string s = FetchWebsiteContent(url);

            htmlDocument.LoadHtml(s);
            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//div[@class='feature']"))
            {
                var newsTitle = node.Descendants("a").Select(n => n.InnerText).ToList();
                var newsLink = node.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                if (newsTitle.Count > 0 && newsLink != "")
                {
                    NewsModel news;
                    if (!newsLink.Contains("reuters.com"))
                        news = new NewsModel(-1, newsTitle[0].ToString(), @"http://www.reuters.com/" + newsLink.ToString(), DayDate, DayDate);
                    else
                        news = new NewsModel(-1, newsTitle[0].ToString(), newsLink.ToString(), DayDate, DayDate);
                    string article = GetReutorsArticleText(news.Url);
                    news.Article = article;
                    ImportantNews.Add(news);
                }
            }
            
            return ImportantNews;


        }


        /// <summary>
        /// This method is responsible for getting news article  from the Guardain  by supplying the URL of the news.After getting all news this method is responisble for getting the artile for each news
        /// It returns an artcile for the specififed news
        /// </summary>
        public string GetGuardainArticleText(String URL)
        {
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                string article="";
                string webContent = FetchWebsiteContent(URL);
                htmlDocument.LoadHtml(webContent);
                if (webContent != "")
                {


                    var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='content__article-body from-content-api js-article__body']");
                    if (spanNode != null)
                    {
                        htmlDocument.LoadHtml(spanNode.InnerHtml);
                        foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                        {
                            article = article + node.InnerText;
                        }

                        

                    }
                  
                }
                return article;
            }
            catch
            {
                return "";
            }


        }


        /// <summary>
        /// This method is responsible for getting news article  from the Reutors  by supplying the URL of the news.After getting all news this method is responisble for getting the artile for each news
        /// It returns the news article
        /// </summary>
        public string  GetReutorsArticleText(string URL)
        {

            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                string webContent = FetchWebsiteContent(URL);
                htmlDocument.LoadHtml(webContent);
                string article = "";
                if (webContent != "")
                {


                    var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//span[@id='articleText']");
                    if (spanNode != null)
                    {
                        
                        htmlDocument.LoadHtml(spanNode.InnerHtml);
                        foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                        {
                            article = article + node.InnerText;
                        }



                    }
                }
                return article;
            }
            catch
            {
                return "";
            }
                   
        }

        #endregion
    }
}