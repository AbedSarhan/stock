﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreeceStockMarketWCF.Controllers.KeyPeopleControllers.WebControllers
{
    public interface WebKeyPersonControllerInterface
    {
        Models.KeyPeople.KeyPersonWebInfoModel FetchPersonInfo(string PersonName, out string Message);
    }
}