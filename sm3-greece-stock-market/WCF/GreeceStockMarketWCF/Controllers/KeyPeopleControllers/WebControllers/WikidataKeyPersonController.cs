﻿using GreeceStockMarketWCF.Controllers.Common.WebControllers;
using GreeceStockMarketWCF.Models.KeyPeople;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace GreeceStockMarketWCF.Controllers.KeyPeopleControllers.WebControllers
{
    public class WikidataKeyPersonController : WebController, WebKeyPersonControllerInterface    
    {

        /// <summary>
        /// This method uses Wikidata url API to fetch keywords related to a specified person
        /// </summary>
        /// <param name="PersonSearchName"></param>
        /// <param name="Message"></param>
        /// <returns>Keywords about a key person</returns>
        public KeyPersonWebInfoModel FetchPersonInfo(string PersonSearchName, out string Message)   
        {
            try
            {
                // Build parameters, run query, and get results
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("action", "wbsearchentities");
                parameters.Add("language", "en");
                parameters.Add("format", "xml");
                parameters.Add("search", PersonSearchName);
                string url = System.Configuration.ConfigurationManager.AppSettings["WebKeyPersonController:WikidataIDSearchURL"];
                string XML = FetchWebsiteContentByGet(url, parameters, out Message);

                // Parse xml doc
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(XML);

                // Make sure there is results
                XmlNode searchResultsNode = xmlDoc.DocumentElement.GetElementsByTagName("search").Item(0);
                if (!searchResultsNode.HasChildNodes)
                {
                    Message = "No results";
                    return null;
                }

                // Find first result about a human
                XmlNode personNode = FindAndFetchPersonNodeFromWeb(searchResultsNode.ChildNodes, out Message);

                // Instantiate the data model root
                string firstResultName = personNode["labels"].FirstChild.Attributes["value"].Value;
                KeyPersonWebInfoModel personModel = new KeyPersonWebInfoModel("person", firstResultName);

                // Go through each claim for this person 
                personModel = FetchEntityClaims(personModel, personNode["claims"], out Message); 

                return personModel;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        /// <summary>
        /// This method retrieves all claims for the person, and stores them in the specified data structure
        /// </summary>
        /// <param name="PersonModel"></param>
        /// <param name="Claims"></param>
        /// <param name="Message"></param>
        /// <returns>The data structure filled with data about the person</returns>
        private KeyPersonWebInfoModel FetchEntityClaims(KeyPersonWebInfoModel PersonModel, XmlNode Claims, out string Message)  
        {
            Message = "";

            foreach (XmlNode property in Claims)
            {
                // Add property
                string propertyName = FetchDataValueFromWeb(property.Attributes["id"].Value, out Message);
                KeyPersonWebInfoModel propertyModel = new KeyPersonWebInfoModel(propertyName, null);

                // Add claims of property
                XmlNodeList innerClaims = property.ChildNodes;
                foreach(XmlNode innerClaim in innerClaims)
                {
                    // Retrieve data type
                    string dataType = innerClaim["mainsnak"]["datavalue"].Attributes["type"].Value;
                    if (dataType == null)
                    {
                        Message = "A data type is null";
                        return new KeyPersonWebInfoModel();
                    }

                    // Retrieve data value
                    string dataValue = null;
                    #region DATA MODEL
                    // From http://www.wikidata.org/wiki/Help:Wikidata_datamodel
                        // wikibase-entityid
                            // value
                                // item
                                // numeric-id
                        // string
                        // time
                            // value
                                // time
                                // precission
                                // before
                                // after
                                // timezone
                                // calendarmodel
                        // monolingualtext
                            // value
                                //text
                                //language
                        // globecoordinate
                            // value
                                // latitude
                                // longitude
                                // precission
                                // globe
                        // quantity
                            // value
                                // amount
                                // lowerbound
                                // upperbound
                        // multilingual text
                    #endregion

                    if (dataType.Equals("wikibase-entityid") || dataType.Equals("item"))
                    {
                        string dataID = "Q" + innerClaim["mainsnak"]["datavalue"]["value"].Attributes["numeric-id"].Value;
                        dataValue = FetchDataValueFromWeb(dataID, out Message);
                    }
                    else if(dataType.Equals("monolingualtext"))
                    {
                        dataValue = innerClaim["mainsnak"]["datavalue"]["value"].Attributes["text"].Value;
                    }
                    else if (dataType.Equals("time"))
                    {
                        dataValue = innerClaim["mainsnak"]["datavalue"]["value"].Attributes["time"].Value;
                    }
                    else 
                    {
                        continue;
                        // TODO: Check how to handle and collect the other data types
                    }

                    KeyPersonWebInfoModel propertyValueModel = propertyModel.AddChild(null, dataValue);

                    // Retrieve sub snaks
                    XmlNodeList qualifierProperties = innerClaim["qualifiers"].ChildNodes;

                    foreach(XmlNode qualifierProperty in qualifierProperties)
                    {
                        string qualifierPropertyName = FetchDataValueFromWeb(qualifierProperty.Attributes["id"].Value, out Message);
                        KeyPersonWebInfoModel qualifierPropertyModel = new KeyPersonWebInfoModel(qualifierPropertyName, null);
   
                        // The N^4 complexity is probably okay, since in most cases there is only 1 sub value, and up to  ~5 in rare cases
                        foreach(XmlNode qualifierValue in qualifierProperty["qualifiers"].ChildNodes)
                        {
                            string snakDataType = qualifierValue.Attributes["type"].Value; // TODO: Save this value?
                            string snakDataValue = "";

                            if (snakDataType.Equals("wikibase-entityid") || dataType.Equals("item"))
                            {
                                string snakDataID = "Q" + qualifierValue["value"].Attributes["numeric-id"].Value;
                                snakDataValue = FetchDataValueFromWeb(snakDataID, out Message);
                            }
                            else if (snakDataType.Equals("monolingualtext"))
                            {
                                snakDataValue = qualifierValue["value"].Attributes["text"].Value;
                            }
                            else if (snakDataType.Equals("time"))
                            {
                                snakDataValue = qualifierValue["value"].Attributes["time"].Value;
                            }
                            else
                            {
                                continue;
                                // TODO: Check how to handle and collect the other data types
                            }
                            qualifierPropertyModel.AddChild(null, snakDataValue);
                        }
                        propertyValueModel.AddChild(qualifierPropertyModel);

                    }
                }

                // Don't add properties that don't have any values (Some may not have values, because I did not want to collect those values. For example, the person's website link)
                if (propertyModel.Children.Count == 0)
                    continue;

                PersonModel.AddChild(propertyModel);

            }

            return PersonModel;
        }

        /// <summary>
        /// This method retrieves the value of a Wikidata item using the ID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Message"></param>
        /// <returns>The values</returns>
        private string FetchDataValueFromWeb(string ID, out string Message)                      
        {
            // Build parameters, run query, and get results
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("action", "wbgetentities");
            parameters.Add("languages", "en");
            parameters.Add("format", "xml");
            parameters.Add("props", "labels");
            parameters.Add("ids", ID);
            string url = System.Configuration.ConfigurationManager.AppSettings["WebKeyPersonController:WikidataIDSearchURL"];
            string XML = FetchWebsiteContentByGet(url, parameters, out Message);

            // Parse xml doc
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNodeList entityNodeList = xmlDoc.GetElementsByTagName("label");

            StringBuilder value = new StringBuilder();

            if (entityNodeList.Count > 0)
                value.Append(entityNodeList.Item(0).Attributes["value"].Value);

            // Concatenate multiple values
            for (int i = 1; i < entityNodeList.Count; i++)
            {
                value.Append(value);
                value.Append(";");
                value.Append(entityNodeList.Item(i).Attributes["value"].Value);
            }

            Message = "";
            return value.ToString();
        }

        /// <summary>
        /// This method returns the first node of type "person", or null otherwise
        /// </summary>
        /// <param name="NodeList"></param>
        /// <param name="Message"></param>
        /// <returns>Returns a node of type "person"</returns>
        private XmlNode FindAndFetchPersonNodeFromWeb(XmlNodeList NodeList, out string Message)    
        {
            foreach (XmlNode node in NodeList)
            {
                string entityID = node.Attributes["id"].Value;

                // Build parameters, run query, and get results
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("action", "wbgetentities");
                parameters.Add("languages", "en");
                parameters.Add("format", "xml");
                parameters.Add("props", "aliases|labels|descriptions|claims");
                parameters.Add("ids", entityID);
                string url = System.Configuration.ConfigurationManager.AppSettings["WebKeyPersonController:WikidataIDSearchURL"];
                string XML = FetchWebsiteContentByGet(url, parameters, out Message);

                // Parse xml doc
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(XML);

                // Get claims for this entity //TODO: Replace search for P31 claim through regex
                XmlNode claimsNode = xmlDoc.GetElementsByTagName("claims").Item(0);
                foreach (XmlNode claim in claimsNode)
                {
                    // Checks for the "human" marker
                    if (claim.Attributes["id"].Value == "P31" && claim["claim"]["mainsnak"]["datavalue"]["value"].Attributes["numeric-id"].Value == "5")
                    {
                        Message = "Person node found";
                        return xmlDoc.GetElementsByTagName("entity")[0];
                    }
                }
            }

            Message = "No person node found";
            return null;
        }

    }
}