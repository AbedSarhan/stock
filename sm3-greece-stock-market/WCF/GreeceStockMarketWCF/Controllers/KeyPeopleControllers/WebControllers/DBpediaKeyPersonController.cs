﻿using GreeceStockMarketWCF.Controllers.Common.WebControllers;
using GreeceStockMarketWCF.Models.KeyPeople;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace GreeceStockMarketWCF.Controllers.KeyPeopleControllers.WebControllers
{
    public class DBpediaKeyPersonController : WebController, WebKeyPersonControllerInterface
    {

        /// <summary>
        /// This method uses the DBpedia Lookup service to fetch keywords related to a specified person
        /// </summary>
        /// <param name="PersonSearchName"></param>
        /// <param name="Message"></param>
        /// <returns>Keywords about a key person</returns>
        public KeyPersonWebInfoModel FetchPersonInfo(string PersonSearchName, out string Message)   
        {
            try
            {
                // Build parameters, run query, and get results
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("QueryClass", "person");
                parameters.Add("QueryString", PersonSearchName);
                string url = System.Configuration.ConfigurationManager.AppSettings["WebKeyPersonController:DBPediaKeywordURL"];
                string XML = FetchWebsiteContentByGet(url, parameters, out Message);

                // Parse xml doc
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(XML);

                // Make sure there is results
                if (!xmlDoc.DocumentElement.HasChildNodes)
                {
                    Message = "No results";
                    return null;
                }
                XmlNodeList results = xmlDoc.DocumentElement.ChildNodes; 

                // Double check that result class is about a person
                XmlNode personNode = FindPersonNode(results, out Message);
                if (personNode == null)
                    return null;

                // Make the node for the person
                string firstResultName = personNode["Label"].InnerText;
                KeyPersonWebInfoModel person = new KeyPersonWebInfoModel("person", firstResultName);

                // Go through each keyword for this person
                foreach (XmlNode childrenNode in personNode["Categories"])
                {
                    string info = childrenNode["Label"].InnerText;
                    person.AddChild(null, info);
                }

                Message = "Successfully retrieved";
                return person;
            }
            catch (Exception exception)
            {
                Message = exception.Message;
                return null;
            }
        }

        /// <summary>
        /// This method returns the first node of type "person", or null otherwise
        /// </summary>
        /// <param name="NodeList"></param>
        /// <param name="Message"></param>
        /// <returns>Returns a node of type "person"</returns>
        private XmlNode FindPersonNode(XmlNodeList NodeList, out string Message)    
        {
            foreach (XmlNode node in NodeList)
            {
                XmlNode classes = node["Classes"];

                foreach (XmlNode thisClass in classes.ChildNodes)
                {
                    if (thisClass["Label"].InnerText == "person")
                    {
                        Message = "Person node found";
                        return node;
                    }
                }
            }

            Message = "No person node found";
            return null;
        }

    }
}