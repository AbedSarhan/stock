﻿using GreeceStockMarketWCF.Controllers.Common.DatabaseControllers;
using GreeceStockMarketWCF.Controllers.Common.FileControllers;
using GreeceStockMarketWCF.Controllers.KeyPeopleControllers.WebControllers;
using GreeceStockMarketWCF.Models.KeyPeople;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GreeceStockMarketWCF
{
    [ServiceContract]
    public class Service
    {
        [OperationContract]
        public bool TestDatabaseConnection(out string Message)
        {
            bool valid = false;

            MSSql2014Controller controller = new MSSql2014Controller();
            valid = controller.OpenConnection(out Message);

            if (valid)
                valid = controller.CloseConnection(out Message);

            return valid;
        }


        [OperationContract]
        public KeyPersonWebInfoModel DBpediaKeyPersonFetcher(string name, out string Message)
        {
            DBpediaKeyPersonController c = new DBpediaKeyPersonController();
            return c.FetchPersonInfo(name, out Message);
        }

        [OperationContract]
        public KeyPersonWebInfoModel DBpediaKeyPersonSaver(string name, out string Message)
        {
            DBpediaKeyPersonController c = new DBpediaKeyPersonController();
            KeyPersonWebInfoModel person = c.FetchPersonInfo(name, out Message);

            if (person != null)
            {
                FileController.SaveFileInXMLFormat("C:\\data\\1.xml", person, new Type[0], out Message);
            }

            return person;
        }

        [OperationContract]
        public KeyPersonWebInfoModel DBpediaKeyPersonLoader(out string Message)
        {
            KeyPersonWebInfoModel person = FileController.LoadFileInXMLFormat("C:\\data\\1.xml", typeof(KeyPersonWebInfoModel), new Type[0], out Message) as KeyPersonWebInfoModel;
            return person;
        }

        [OperationContract]
        public KeyPersonWebInfoModel WikidataKeyPersonFetcher(string name, out string Message)
        {
            WikidataKeyPersonController c = new WikidataKeyPersonController();
            return c.FetchPersonInfo(name, out Message);
        }

        [OperationContract]
        public KeyPersonWebInfoModel WikidataKeyPersonSaver(string name, out string Message)
        {
            WikidataKeyPersonController c = new WikidataKeyPersonController();
            KeyPersonWebInfoModel person = c.FetchPersonInfo(name, out Message);

            if (person != null)
            {
                FileController.SaveFileInXMLFormat("C:\\data\\1.xml", person, new Type[0], out Message);
            }

            return person;
        }

        [OperationContract]
        public KeyPersonWebInfoModel WikidataKeyPersonLoader(out string Message)
        {
            return FileController.LoadFileInXMLFormat("C:\\data\\1.xml", typeof(KeyPersonWebInfoModel), new Type[0], out Message) as KeyPersonWebInfoModel;
        }
    }
}
