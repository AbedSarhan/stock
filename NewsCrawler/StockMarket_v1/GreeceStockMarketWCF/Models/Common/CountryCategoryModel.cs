﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreeceStockMarketWCF.Models.Common
{
    public class CountryCategoryModel
    {

        #region Constructors

        public CountryCategoryModel(int ID, string Name, string Description) 
        {
            this.ID = ID;
            this.Name = Name;
            this.Description = Description;
        }

        #endregion

        #region Variables

        private int id;
        private string name;

        private string description;

        #endregion

        #region Properties

        /// <summary>
        /// ID generated in our system for the category
        /// </summary>
        public int ID               
        {
            get { return this.id; }
            set { this.id = value; }
        }
        /// <summary>
        /// Name of the category
        /// </summary>
        public string Name          
        {
            get { return this.name; }
            set { this.name = value; }
        }



        /// <summary>
        /// Description about this category and its importance
        /// </summary>
        public string Description   
        {
            get { return this.description; }
            set { this.description = value; }
        }

        #endregion

    }
}