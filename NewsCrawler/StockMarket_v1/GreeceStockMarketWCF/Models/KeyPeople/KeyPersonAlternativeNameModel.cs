﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreeceStockMarketWCF.Models.KeyPeople
{
    public class KeyPersonAlternativeNameModel
    {

        #region Constructors

        public KeyPersonAlternativeNameModel(string Name, string Notes) 
        {
            this.Name = Name;
            this.Notes = Notes;
        }

        #endregion

        #region Variables

        private string name;
        private string notes;

        #endregion

        #region Properties

        /// <summary>
        /// Alternative name
        /// </summary>
        public string Name  
        {
            get { return this.name; }
            set { this.name = value; }
        }


        /// <summary>
        /// Notes (such as the api which uses this name as an identifier)
        /// </summary>
        public string Notes 
        {
            get { return this.notes; }
            set { this.notes = value; }
        }

        #endregion

    }
}