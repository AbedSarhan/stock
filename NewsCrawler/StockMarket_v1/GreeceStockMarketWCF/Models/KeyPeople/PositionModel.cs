﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreeceStockMarketWCF.Models.KeyPeople
{
    public class PositionModel
    {

        #region Constructors

        public PositionModel(ImportantTitleModel Title, DateTime StartDate, DateTime EndDate)   
        {
            this.Title = Title;

            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        #endregion

        #region Variables

        private ImportantTitleModel title;

        private DateTime startDate;
        private DateTime endDate;

        #endregion

        #region Properties

        /// <summary>
        /// Important title that is being occupied by this position
        /// </summary>
        public ImportantTitleModel Title    
        {
            get { return this.title; }
            set { this.title = value; }
        }



        /// <summary>
        /// Starting Date of the occupation
        /// </summary>
        public DateTime StartDate   
        {
            get { return this.startDate; }
            set { this.startDate = value; }
        }
        /// <summary>
        /// Ending Date of the occupation, (it can also contain prediction dates of when it is expected to end)
        /// </summary>
        public DateTime EndDate     
        {
            get { return this.endDate; }
            set { this.endDate = value; }
        }

        #endregion

    }
}