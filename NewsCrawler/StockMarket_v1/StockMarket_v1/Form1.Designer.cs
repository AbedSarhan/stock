﻿namespace StockMarket_v1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Indicesddl = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewIndexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importIndexInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importCompanyInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.importSymbolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Indicesddl
            // 
            this.Indicesddl.FormattingEnabled = true;
            this.Indicesddl.Location = new System.Drawing.Point(114, 87);
            this.Indicesddl.Name = "Indicesddl";
            this.Indicesddl.Size = new System.Drawing.Size(298, 24);
            this.Indicesddl.TabIndex = 0;
            this.Indicesddl.SelectedIndexChanged += new System.EventHandler(this.Indicesddl_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Indices";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(12, 184);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(279, 308);
            this.listBox1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Companies";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(882, 28);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewIndexToolStripMenuItem,
            this.importIndexInfoToolStripMenuItem,
            this.importCompanyInfoToolStripMenuItem,
            this.importSymbolsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // addNewIndexToolStripMenuItem
            // 
            this.addNewIndexToolStripMenuItem.Name = "addNewIndexToolStripMenuItem";
            this.addNewIndexToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.addNewIndexToolStripMenuItem.Text = "Import Companies";
            this.addNewIndexToolStripMenuItem.Click += new System.EventHandler(this.addNewIndexToolStripMenuItem_Click);
            // 
            // importIndexInfoToolStripMenuItem
            // 
            this.importIndexInfoToolStripMenuItem.Name = "importIndexInfoToolStripMenuItem";
            this.importIndexInfoToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.importIndexInfoToolStripMenuItem.Text = "Import Index Info";
            this.importIndexInfoToolStripMenuItem.Click += new System.EventHandler(this.importIndexInfoToolStripMenuItem_Click);
            // 
            // importCompanyInfoToolStripMenuItem
            // 
            this.importCompanyInfoToolStripMenuItem.Name = "importCompanyInfoToolStripMenuItem";
            this.importCompanyInfoToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.importCompanyInfoToolStripMenuItem.Text = "Import Company Info";
            this.importCompanyInfoToolStripMenuItem.Click += new System.EventHandler(this.importCompanyInfoToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(114, 134);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 7;
            // 
            // importSymbolsToolStripMenuItem
            // 
            this.importSymbolsToolStripMenuItem.Name = "importSymbolsToolStripMenuItem";
            this.importSymbolsToolStripMenuItem.Size = new System.Drawing.Size(220, 24);
            this.importSymbolsToolStripMenuItem.Text = "Import Symbols";
            this.importSymbolsToolStripMenuItem.Click += new System.EventHandler(this.importSymbolsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 507);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Indicesddl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Stock Market";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Indicesddl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewIndexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importIndexInfoToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem importCompanyInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importSymbolsToolStripMenuItem;
    }
}

