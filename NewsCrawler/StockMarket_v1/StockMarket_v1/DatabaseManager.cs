﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace StockMarket_v1
{
    public class DatabaseManager
    {

        #region Query Executer
        private static SqlConnection sqlConn;
        private static SqlCommand cmd;
        public static string Get_ConnectionString()
        {
            try
            {
                return GlobalVaraibles.DatabaseServer;
            }
            catch { return null; }
        }
        public static SqlConnection GetSQLConnection()
        {
            if (Get_ConnectionString() == null)
                return null;
            return new SqlConnection(Get_ConnectionString());
        }



        public static int Execute_Non_Query_Store_Procedure(string procedureName, SqlParameter[] parameters)
        {
            if (GetSQLConnection() == null)
                return -1;

            int successfulQuery = -1;
            SqlCommand sqlCommand = new SqlCommand(procedureName, GetSQLConnection());
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.Connection.Open();
                successfulQuery = sqlCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            if (sqlCommand.Connection != null && sqlCommand.Connection.State == ConnectionState.Open)
                sqlCommand.Connection.Close();

            return successfulQuery;
        }

        public static DataTable Execute_Data_Query_Store_Procedure(string procedureName, SqlParameter[] parameters)
        {
            if (GetSQLConnection() == null)
                return null;

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(procedureName, GetSQLConnection());
            sqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlAdapter.SelectCommand.Parameters.AddRange(parameters);
                sqlAdapter.SelectCommand.Connection.Open();
                sqlAdapter.Fill(dataTable);
            }
            catch
            { dataTable = null; }

            if (sqlAdapter.SelectCommand.Connection != null && sqlAdapter.SelectCommand.Connection.State == ConnectionState.Open)
                sqlAdapter.SelectCommand.Connection.Close();

            return dataTable;
        }


        public static DataSet ExecuteDataSetQueryStoreProcedure(string procedureName, SqlParameter[] parameters)
        {
            if (GetSQLConnection() == null)
                return null;

            DataSet DS = new DataSet();
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(procedureName, GetSQLConnection());
            sqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlAdapter.SelectCommand.Parameters.AddRange(parameters);
                sqlAdapter.SelectCommand.Connection.Open();
                sqlAdapter.Fill(DS);
            }
            catch
            { DS = null; }

            if (sqlAdapter.SelectCommand.Connection != null && sqlAdapter.SelectCommand.Connection.State == ConnectionState.Open)
                sqlAdapter.SelectCommand.Connection.Close();

            return DS;
        }
        #endregion



        public static List<StockMarketIndex> GetStockMarketsIndices()
        {
            DataTable dt = new DataTable();
            List<StockMarketIndex> smi = new List<StockMarketIndex>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return null;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.GetStockIndices";

                sqlConn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);


                da.Fill(dt);
                cmd.Connection.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    smi.Add(new StockMarketIndex(int.Parse(dr["stockMarketID"].ToString()), int.Parse(dr["StockMarketIndexID"].ToString()), dr["IndexName"].ToString(), dr["Symbol"].ToString()));
                }
                return smi;


            }
            catch { sqlConn.Close(); return null; }


        }

        public static List<StockMarketCompany> GetStockMarketCompany(int indexID)
        {

            DataTable dt = new DataTable();
            List<StockMarketCompany> smc = new List<StockMarketCompany>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return null;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.GetStockIndices";

                sqlConn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);


                da.Fill(dt);
                cmd.Connection.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    smc.Add(new StockMarketCompany(int.Parse(dr["CompanyID"].ToString()), int.Parse(dr["stockMarketIndexId"].ToString()), dr["companyName"].ToString(), dr["indexSymbol"].ToString()));
                }
                return smc;


            }
            catch { sqlConn.Close(); return null; }

        }

        public static bool AddIndexCompany(int stockMarketIndexId, string companyName, string indexSymbol)
        {

            DataTable dt = new DataTable();
            List<StockMarketCompany> smc = new List<StockMarketCompany>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.AddIndexCOmpany";
                cmd.Parameters.AddWithValue("@stockMarketIndexId", stockMarketIndexId);
                cmd.Parameters.AddWithValue("@companyName", companyName);
                cmd.Parameters.AddWithValue("@indexSymbol", indexSymbol);


                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }


        }

        public static bool AddIndexSnapshot(DateTime SnapshotDate, int StockMarketindexID, decimal PpenVal, decimal CloseVal, decimal HighVal, decimal LowVal, decimal Volume, decimal AdjClose)
        {
            DataTable dt = new DataTable();
            List<StockMarketCompany> smc = new List<StockMarketCompany>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.AddIndexSnapshot";
                cmd.Parameters.AddWithValue("@SnapshotDate", SnapshotDate);
                cmd.Parameters.AddWithValue("@StockMarketindexID", StockMarketindexID);
                cmd.Parameters.AddWithValue("@PpenVal", PpenVal);
                cmd.Parameters.AddWithValue("@CloseVal", CloseVal);
                cmd.Parameters.AddWithValue("@HighVal", HighVal);
                cmd.Parameters.AddWithValue("@LowVal", LowVal);
                cmd.Parameters.AddWithValue("@AdjClose", AdjClose);
                cmd.Parameters.AddWithValue("@Volume", Volume);

                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }

        public static bool UpdateIndexSnapshot(int snapshotID, decimal volumeChangePercentage,decimal closingChangePercentage)
        {
            DataTable dt = new DataTable();
            List<StockMarketCompany> smc = new List<StockMarketCompany>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.UpdateIndexSnapshot";
                cmd.Parameters.AddWithValue("@SnapshotID", snapshotID);
                cmd.Parameters.AddWithValue("@VOlumePercentage", volumeChangePercentage);
                cmd.Parameters.AddWithValue("@closePercentage", closingChangePercentage);


                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }

        public static bool AddCompanySnapshot(DateTime SnapshotDate, int StockMarketCompanyID, decimal PpenVal, decimal CloseVal, decimal HighVal, decimal LowVal, decimal Volume, decimal AdjClose)
        {
            DataTable dt = new DataTable();
            List<StockMarketCompany> smc = new List<StockMarketCompany>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.[AddCompanySnapshot]";
                cmd.Parameters.AddWithValue("@SnapshotDate", SnapshotDate);
                cmd.Parameters.AddWithValue("@StockMarketCompanyID", StockMarketCompanyID);
                cmd.Parameters.AddWithValue("@PpenVal", PpenVal);
                cmd.Parameters.AddWithValue("@CloseVal", CloseVal);
                cmd.Parameters.AddWithValue("@HighVal", HighVal);
                cmd.Parameters.AddWithValue("@LowVal", LowVal);
                cmd.Parameters.AddWithValue("@AdjClose", AdjClose);
                cmd.Parameters.AddWithValue("@Volume", Volume);

                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }

        public static bool AddCompanySymbol(String CompanyName, string Symbol)
        {

            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.UpdateSymbol";
                cmd.Parameters.AddWithValue("@companyName", CompanyName);
                cmd.Parameters.AddWithValue("@Symbol", Symbol);


                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }

        public static bool AddNews(String newsURL, string NewsTitle,DateTime Newsdate)
        {

            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.AddNews";
                cmd.Parameters.AddWithValue("@NewsURL", newsURL);
                cmd.Parameters.AddWithValue("@NewsTitle", NewsTitle);
                string d = Newsdate.ToShortDateString();
                cmd.Parameters.AddWithValue("@NewsDate", Newsdate);

                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }

        public static bool AddGreeceNews(Tuple<string, string, string, string, string, string> gNews)
        {

            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sqlConn.Open();
                
                    cmd.CommandText = "dbo.AddGreeceNews";
                    cmd.Parameters.AddWithValue("@NewsTitle", gNews.Item2);
                    cmd.Parameters.AddWithValue("@NewsURL", gNews.Item3);
                    cmd.Parameters.AddWithValue("@NewsDate", gNews.Item1);
                    cmd.Parameters.AddWithValue("@NewsAuthor", gNews.Item4);
                    cmd.Parameters.AddWithValue("@NewsText", gNews.Item5);
                    cmd.Parameters.AddWithValue("@publisher", gNews.Item6);
                    cmd.ExecuteNonQuery();
                
               
               

               
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }

        public static DataTable GetCompaniesWithNoData()
        {
            DataTable dt = new DataTable();
            List<StockMarketCompany> smc = new List<StockMarketCompany>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return null;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.GetCOmpaniesWithNoData";

                sqlConn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);


                da.Fill(dt);
                cmd.Connection.Close();

                return dt;
            }
            catch { sqlConn.Close(); return null; }
        }


        public static  List<StockIndexData> GetStockIndexData(int indexID)
        {
            DataTable dt = new DataTable();
            List<StockIndexData> sid = new List<StockIndexData>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return null;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.GetStockIndexesData";
                cmd.Parameters.AddWithValue("@indexID", indexID);

                sqlConn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);


                da.Fill(dt);
                cmd.Connection.Close();

                foreach(DataRow dr in dt.Rows)
                {
                    DateTime dt1 = DateTime.Parse(dr["SnapshotDate"].ToString());
                    Decimal volume = Decimal.Parse(dr["Volume"].ToString().Trim());
                    Decimal open = Decimal.Parse(dr["PpenVal"].ToString().Trim());
                    Decimal close = Decimal.Parse(dr["CloseVal"].ToString().Trim());
                    Decimal high = Decimal.Parse(dr["HighVal"].ToString().Trim());
                    Decimal low = Decimal.Parse(dr["LowVal"].ToString().Trim());
                    Decimal adjacentClose = Decimal.Parse(dr["AdjClose"].ToString().Trim());
                    sid.Add(new StockIndexData(int.Parse(dr["SnapshotID"].ToString()), dt1, open, close, high, low, adjacentClose, volume));
                }

                return sid;
            }
            catch { sqlConn.Close(); return null; }


        }

        public static List<News> GetNews()
        {
            DataTable dt = new DataTable();
            List<News> news = new List<News>();
            cmd = new SqlCommand();
            try
            {


                if (GetSQLConnection() == null)
                    return null;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.GetNews";

                sqlConn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);


                da.Fill(dt);
                cmd.Connection.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    news.Add(new News(DateTime.Now, dr[1].ToString(), "", "",int.Parse(dr[0].ToString())));
                }

                return news;
            }
            catch { sqlConn.Close(); return null; }


        }
          public static bool UpdateNewsScore(int newsID,decimal score)
        {
            cmd = new SqlCommand();
            try
            {

                if (GetSQLConnection() == null)
                    return false;
                sqlConn = GetSQLConnection();
                cmd.Connection = sqlConn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "dbo.updateNewsScore";
                cmd.Parameters.AddWithValue("@NewsID", newsID);
                cmd.Parameters.AddWithValue("@score", score);

                sqlConn.Open();

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                return true;


            }
            catch { sqlConn.Close(); return false; }
        }
    }
}
