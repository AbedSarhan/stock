﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockMarket_v1
{
    public class StockMarketIndex
    {
        #region Varaibles 

        private int stockMarketIndexID;

       
        private int stockMarketID;

       
        private String indexName;

       
        private String symbol;
        #endregion


        #region Properties
        public String Symbol
        {
            get { return symbol; }
           
        }
        public int StockMarketIndexID
        {
            get { return stockMarketIndexID; }
          
        }
        public String IndexName
        {
            get { return indexName; }
          
        }
        public int StockMarketID
        {
            get { return stockMarketID; }
           
        }

        #endregion


        public StockMarketIndex(int stockMarketID, int stockMarketIndexID, string symbol, string indexName)
        {
            this.symbol = symbol;
            this.stockMarketID = stockMarketID;
            this.indexName = indexName;
            this.stockMarketIndexID = stockMarketIndexID;
        }

        public override string ToString()
        {

            return indexName+ "("+Symbol+")";
        }
    }
}
