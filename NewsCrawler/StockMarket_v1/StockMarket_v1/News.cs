﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockMarket_v1
{
    public class News
    {

        DateTime newsDate;       
        String newsTitle;       
        String newsUrl;      
        String newsArticle;
        int newsID;

       

        public String NewsArticle
        {
            get { return newsArticle; }
            set { newsArticle = value; }
        }
        public DateTime NewsDate
        {
            get { return newsDate; }            
        }
        public String NewsUrl
        {
            get { return newsUrl; }            
        }
        public String NewsTitle
        {
            get { return newsTitle; }
        }
        public int NewsID
        {
            get { return newsID; }
            set { newsID = value; }
        }
        public News(DateTime newsDate,String newsTitle, String newsUrl,String newsArticle,int id)
        {
            this.newsDate = newsDate;
            this.newsTitle = newsTitle;
            this.newsUrl = newsUrl;
            this.newsArticle = newsArticle;
            this.newsID = id;
        }
    }
}
