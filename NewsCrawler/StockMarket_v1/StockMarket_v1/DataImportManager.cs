﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using HtmlAgilityPack;
using System.Data;
using System.Globalization;
using System.Collections;
using System.Text.RegularExpressions;


namespace StockMarket_v1
{
    public class DataImportManager
    {

        #region Update companies
        public bool ImportCompanies(string FileName, int index)
        {


            StreamReader rules = new StreamReader(FileName);

            string line = "";
            while ((line = rules.ReadLine()) != null)
            {
                string[] val = line.Split(',');
                val[0] = val[0].Replace('(', ',');
                val[0] = val[0].Replace(')', ' ');
                string[] val2 = val[0].Split(',');

                DatabaseManager.AddIndexCompany(index, val2[0].Trim(), val2[1].Trim());
            }
            return true;
        }

        #endregion



        #region Update stocks from csv  file
        public bool ImportIndexStockDataFromFile(string FileName, int index)
        {
            try
            {
                StreamReader rules = new StreamReader(FileName);

                string line = "";
                rules.ReadLine();
                while ((line = rules.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    if (!string.IsNullOrEmpty(val[0].ToString()))
                    {

                        DateTime date = DateTime.Parse(val[0].ToString());
                        decimal open = decimal.Parse(val[1].ToString());
                        decimal high = decimal.Parse(val[2].ToString());
                        decimal low = decimal.Parse(val[3].ToString());
                        decimal close = decimal.Parse(val[4].ToString());
                        decimal vol = decimal.Parse(val[5].ToString());
                        decimal adjvol = decimal.Parse(val[6].ToString());
                        DatabaseManager.AddIndexSnapshot(date, index, open, close, high, low, vol, adjvol);
                    }
                }
                rules.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ImportCOmpanyStockDataFromFile(string FileName, int companyID)
        {
            try
            {
                StreamReader rules = new StreamReader(FileName);

                string line = "";
                rules.ReadLine();
                while ((line = rules.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    DateTime date = DateTime.Parse(val[0].ToString());
                    decimal open = decimal.Parse(val[1].ToString());
                    decimal high = decimal.Parse(val[2].ToString());
                    decimal low = decimal.Parse(val[3].ToString());
                    decimal close = decimal.Parse(val[4].ToString());
                    decimal vol = decimal.Parse(val[5].ToString());
                    decimal adjvol = decimal.Parse(val[6].ToString());
                    DatabaseManager.AddCompanySnapshot(date, companyID, open, close, high, low, vol, adjvol);
                }
                rules.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion


        #region Update stock data Online
        public bool ImportIndexStockDataOnline(string indexSymbol, int companyID)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + indexSymbol);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string line = "";
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                if (sr != null)
                {
                    sr.ReadLine();
                    while ((line = sr.ReadLine()) != null)
                    {
                        List<string> val = line.Split(',').ToList<string>();
                        DateTime date = DateTime.Parse(val[0].ToString());
                        decimal open = decimal.Parse(val[1].ToString());
                        decimal high = decimal.Parse(val[2].ToString());
                        decimal low = decimal.Parse(val[3].ToString());
                        decimal close = decimal.Parse(val[4].ToString());
                        decimal vol = decimal.Parse(val[5].ToString());
                        decimal adjvol = decimal.Parse(val[6].ToString());
                        DatabaseManager.AddCompanySnapshot(date, companyID, open, close, high, low, vol, adjvol);
                    }

                    return true;
                }
                else
                    return false;

            }
            catch (Exception er)
            {
                return false;
            }
        }

        //The thrid bool varaiable is to see if the item is index or comany.If yes then the item is index other wise is company
        public List<Tuple<string, int, bool>> UpdateAllStockMarketDataOnline()
        {
            List<StockMarketIndex> indices = DatabaseManager.GetStockMarketsIndices();
            List<Tuple<string, int, bool>> unretrievedresults = new List<Tuple<string, int, bool>>();
            foreach (StockMarketIndex smi in indices)
            {
                List<StockMarketCompany> companies = DatabaseManager.GetStockMarketCompany(smi.StockMarketIndexID);
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + smi.Symbol);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string line = "";
                StreamReader sr = new StreamReader(resp.GetResponseStream());

                sr.ReadLine();
                int counter = 0;
                if ((line = sr.ReadLine()) != null)
                {
                    counter++;
                    List<string> val = line.Split(',').ToList<string>();
                    DateTime date = DateTime.Parse(val[0].ToString());
                    decimal open = decimal.Parse(val[1].ToString());
                    decimal high = decimal.Parse(val[2].ToString());
                    decimal low = decimal.Parse(val[3].ToString());
                    decimal close = decimal.Parse(val[4].ToString());
                    decimal vol = decimal.Parse(val[5].ToString());
                    decimal adjvol = decimal.Parse(val[6].ToString());
                    DatabaseManager.AddIndexSnapshot(date, smi.StockMarketIndexID, open, close, high, low, vol, adjvol);
                }
                if (counter == 0)
                    unretrievedresults.Add(new Tuple<string, int, bool>(smi.Symbol, smi.StockMarketIndexID, true));
                foreach (StockMarketCompany smc in companies)
                {
                    req = (HttpWebRequest)WebRequest.Create(@"http://ichart.yahoo.com/table.csv?s=" + smc.IndexSymbol);
                    resp = (HttpWebResponse)req.GetResponse();

                    line = "";
                    sr = new StreamReader(resp.GetResponseStream());

                    sr.ReadLine();
                    int companycounter = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        companycounter++;
                        List<string> val = line.Split(',').ToList<string>();
                        DateTime date = DateTime.Parse(val[0].ToString());
                        decimal open = decimal.Parse(val[1].ToString());
                        decimal high = decimal.Parse(val[2].ToString());
                        decimal low = decimal.Parse(val[3].ToString());
                        decimal close = decimal.Parse(val[4].ToString());
                        decimal vol = decimal.Parse(val[5].ToString());
                        decimal adjvol = decimal.Parse(val[6].ToString());
                        DatabaseManager.AddCompanySnapshot(date, smc.CompreanyID, open, close, high, low, vol, adjvol);
                    }
                    if (companycounter == 0)
                        unretrievedresults.Add(new Tuple<string, int, bool>(smc.IndexSymbol, smc.CompreanyID, false));
                }

            }


            return unretrievedresults;
        }
        #endregion


        #region Fetch Website Content
        private string FetchWebsiteContent(string URL)
        {
            try
            {
                return new System.Net.WebClient().DownloadString(URL);
            }
            catch
            {
                return "";
            }
        }

        #endregion


        #region Get Reuters and Guardain News
        public List<News> getGuardainData(DateTime fdate, DateTime tdate)
        {
            List<News> ImportantNews = new List<News>();
            string fdateparsed = "" + fdate.Year.ToString() + "-" + fdate.Month.ToString() + "-" + fdate.Day.ToString();
            string tdateparsed = "" + tdate.Year.ToString() + "-" + tdate.Month.ToString() + "-" + tdate.Day.ToString();
            string url = @"http://content.guardianapis.com/search?q=World%20news&api-key=erst3x6w98kw9vfnk48tnxdr&page-size=200&from-date=2014-01-01&to-date=2014-01-01";

            string html = FetchWebsiteContent(url);
            JObject websiteResponse = JObject.Parse(html);
            JObject response = (JObject)websiteResponse["response"];
            JValue validLogin = (JValue)response["status"].ToString();
            if (validLogin.Value.ToString().ToLower() == "ok")
            {
                JArray res = (JArray)response["results"];
                foreach (JObject obj in res)
                {
                    if (obj["sectionName"].ToString().ToLower().Contains("new") || obj["sectionName"].ToString().ToLower().Contains("politics") || obj["sectionName"].ToString().ToLower().Contains("technology"))
                    {
                        ImportantNews.Add(new News(fdate, obj["webTitle"].ToString(), obj["webUrl"].ToString(), "", -1));

                    }

                }
                GetGuardainArticleText(ImportantNews);
                return ImportantNews;
            }
            else
                return null;

        }
        public List<News> GetReutorsNews(DateTime fdate, DateTime tdate)
        {
            List<News> ImportantNews = new List<News>();
            string fdateparsed = String.Format("{0:D2}{1:D2}{2:D2}", fdate.Month, fdate.Day, fdate.Year);
            string tdateparsed = String.Format("{0:D2}{1:D2}{2:D2}", fdate.Month, fdate.Day, fdate.Year);

            string url = @"http://www.reuters.com/news/archive/worldNews?date=" + fdateparsed;
            HtmlDocument htmlDocument = new HtmlDocument();
            string s = FetchWebsiteContent(url);
            htmlDocument.LoadHtml(s);
            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//div[@class='feature']"))
            {
                var newsTitle = node.Descendants("a").Select(n => n.InnerText).ToList();
                var newsLink = node.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                if (newsTitle.Count > 0 && newsLink != "")
                {
                    News news;
                    if (!newsLink.Contains("reuters.com"))
                        news = new News(tdate, newsTitle[0].ToString(), @"http://www.reuters.com/" + newsLink.ToString(), "", -1);
                    else
                        news = new News(tdate, newsTitle[0].ToString(), newsLink.ToString(), "", -1);
                    ImportantNews.Add(news);
                }
            }
            //GetReutorsArticleText(ImportantNews);
            //imporNews(ImportantNews);
            return ImportantNews;


        }
        public void imporNews(List<News> news)
        {
            if (news.Count > 0)
            {
                for (int i = 0; i < news.Count(); i++)
                {
                    DatabaseManager.AddNews(news[i].NewsUrl, news[i].NewsTitle, news[i].NewsDate);
                }
            }
        }

        public List<News> GetGuardainArticleText(List<News> GuardainNews)
        {
            if (GuardainNews.Count > 0)
            {
                for (int i = 0; i < GuardainNews.Count(); i++)
                {
                    News n = GuardainNews[i];
                    HtmlDocument htmlDocument = new HtmlDocument();
                    string s = FetchWebsiteContent(n.NewsUrl);
                    htmlDocument.LoadHtml(s);
                    if (s != "")
                    {


                        var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='content__article-body from-content-api js-article__body']");
                        if (spanNode != null)
                        {
                            string article = "";
                            htmlDocument.LoadHtml(spanNode.InnerHtml);
                            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                            {
                                article = article + node.InnerText;
                            }
                            GuardainNews[i].NewsArticle = article;


                        }
                    }
                    else
                        GuardainNews[i].NewsArticle = "Article was moved by Guardain";

                }
                return GuardainNews;
            }
            else
                return null;
        }



        public List<News> GetReutorsArticleText(List<News> ReutorsNews)
        {
            if (ReutorsNews.Count > 0)
            {
                for (int i = 0; i < ReutorsNews.Count(); i++)
                {
                    News n = ReutorsNews[i];
                    HtmlDocument htmlDocument = new HtmlDocument();
                    string s = FetchWebsiteContent(n.NewsUrl);
                    htmlDocument.LoadHtml(s);
                    if (s != "")
                    {


                        var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//span[@id='articleText']");
                        if (spanNode != null)
                        {
                            string article = "";
                            htmlDocument.LoadHtml(spanNode.InnerHtml);
                            foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                            {
                                article = article + node.InnerText;
                            }
                            ReutorsNews[i].NewsArticle = article;


                        }
                    }
                    else
                        ReutorsNews[i].NewsArticle = "Article was moved by Reutors";

                }
                return ReutorsNews;
            }
            else
                return null;
        }

        #endregion


        #region Import symbols

        public bool ImportSymbols(string fileName)
        {
            //UpdateSymbol

            try
            {
                StreamReader rules = new StreamReader(fileName);

                string line = "";
                rules.ReadLine();
                while ((line = rules.ReadLine()) != null)
                {
                    List<string> val = line.Split(',').ToList<string>();
                    DatabaseManager.AddCompanySymbol(val[0].Trim(), val[1].Trim());
                    //DatabaseManager.AddCompanySymbol(val[3].Trim(), val[4].Trim());
                }
                rules.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        #endregion

        #region GetDataFOrMissingSymbols
        public bool GetCompaniesWithNoData()
        {
            Dictionary<int, string> notImported = new Dictionary<int, string>();
            DataTable dt = DatabaseManager.GetCompaniesWithNoData();
            if (dt == null)
                return false;
            foreach (DataRow dr in dt.Rows)
            {
                int companyID = int.Parse(dr["CompanyID"].ToString());
                String symbol = dr["indexSymbol"].ToString();
                bool imported = ImportIndexStockDataOnline(symbol, companyID);
                if (!imported)
                {
                    notImported.Add(companyID, dr["companyName"].ToString());

                }

            }
            return true;
        }

        #endregion

        #region Calculate Important Dates weekly  First Module

        public void ImportnatDate(int indexID)
        {
            Dictionary<int, ArrayList> weekFinalgroups = new Dictionary<int, ArrayList>();
            ArrayList ImportantDates = new ArrayList();

            #region group snapshot into week groups
            List<StockIndexData> sid = DatabaseManager.GetStockIndexData(indexID);
            var weekGroups = sid
            .Select(p => new
            {
                stockInfo = p,
                Year = p.SnapShotDate.Year,
                Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear
                              (p.SnapShotDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
            })
            .GroupBy(x => new { x.Year, x.Week })
            .Select((g, i) => new
            {
                WeekGroup = g,
                WeekNum = i + 1,
                Year = g.Key.Year,
                CalendarWeek = g.Key.Week
            });
            #endregion

            #region Read data grom the ggrouped result and then parse the date
            foreach (var projGroup in weekGroups)
            {
                Console.WriteLine("Week " + projGroup.WeekNum);
                foreach (var proj in projGroup.WeekGroup)
                {
                    if (!weekFinalgroups.ContainsKey(projGroup.WeekNum))
                    {
                        ArrayList ar = new ArrayList();
                        ar.Add(new StockIndexData(proj.stockInfo.SnapshotID, proj.stockInfo.SnapShotDate, proj.stockInfo.Open, proj.stockInfo.Close, proj.stockInfo.High, proj.stockInfo.Low, proj.stockInfo.AdjClose, proj.stockInfo.Volume));
                        weekFinalgroups.Add(projGroup.WeekNum, ar);
                    }
                    else
                    {
                        weekFinalgroups[projGroup.WeekNum].Add(new StockIndexData(proj.stockInfo.SnapshotID, proj.stockInfo.SnapShotDate, proj.stockInfo.Open, proj.stockInfo.Close, proj.stockInfo.High, proj.stockInfo.Low, proj.stockInfo.AdjClose, proj.stockInfo.Volume));
                    }
                }



            }
            #endregion
            #region COMpare the start open of the week and the closeing at the end ot the week for the stok if it difference in -2 the dates of this week as important
            foreach (int key in weekFinalgroups.Keys)
            {
                ArrayList stocks = weekFinalgroups[key];
                StockIndexData firstWeekDay = (StockIndexData)stocks[0];
                StockIndexData LastWeekDay = (StockIndexData)stocks[stocks.Count - 1];

                if (Math.Abs(firstWeekDay.Open - LastWeekDay.Close) >= 500)
                {
                    ImportantDates.AddRange(stocks);
                }

            }


            #endregion
        }


        #endregion

        #region ImportantDatesBased on Volume
        public void ComputeVOlumePercentages()
        {
            List<StockMarketIndex> sotckIndexes = DatabaseManager.GetStockMarketsIndices();
            foreach (StockMarketIndex smi in sotckIndexes)
            {
                List<StockIndexData> sid = DatabaseManager.GetStockIndexData(smi.StockMarketIndexID);
                for (int i = 1; i < sid.Count; i++)
                {

                    decimal volumePercentage = 0;
                    if (sid[i - 1].Volume != 0 && sid[i].Volume != 0)
                        volumePercentage = ((sid[i - 1].Volume - sid[i].Volume) * 100) / sid[i - 1].Volume;
                    decimal closePercentage = 0;
                    if (sid[i - 1].Close != 0 && sid[i].Close != 0)
                        closePercentage = ((sid[i - 1].Close - sid[i].Close) * 100) / sid[i - 1].Close;
                    DatabaseManager.UpdateIndexSnapshot(sid[i].SnapshotID, volumePercentage, closePercentage);

                }

            }
        }

        #endregion

        #region Calculate Important Dates weekly  Second and third Module

        public void ImportnatDate2(int indexID)
        {
            Dictionary<int, ArrayList> weekFinalgroups = new Dictionary<int, ArrayList>();
            ArrayList ImportantDates = new ArrayList();

            #region group snapshot into week groups
            List<StockIndexData> sid = DatabaseManager.GetStockIndexData(indexID);
            for (int i = 0; i < sid.Count(); i++)
            {
                if (i < sid.Count() - 1)
                {
                    if (Math.Abs(sid[i].Open - sid[i].Close) > 300 || Math.Abs(sid[i].Open - sid[i + 1].Open) > 200)
                    {
                        if (!ImportantDates.Contains(sid[i]))
                            ImportantDates.Add(sid[i]);
                        if (!ImportantDates.Contains(sid[i + 1]))
                            ImportantDates.Add(sid[i + 1]);

                    }
                }
            }

            #endregion

        }


        #endregion

        #region Fecth Greece News From http://www.greekcrisis.net/2015/09

        public List<Tuple<string, string, string, string, string, string>> FetchGreecNews()
        {
            List<Tuple<string, string, string, string, string, string>> news = new List<Tuple<string, string, string, string, string, string>>();
            int counter = 0;
            int counte = 0;
            for (int year = 2015; year > 2006; year--)
            {

                for (int i = (year == 2015 ? 10 : 12); i > 0; i--)
                {
                    string display = i.ToString("00");
                    string url = @"http://www.greekcrisis.net/" + year + "/" + display;

                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.OptionDefaultStreamEncoding = Encoding.UTF8;
                    htmlDocument.OptionReadEncoding = true;
                    string s = FetchWebsiteContent(url);
                    string k = System.Net.WebUtility.HtmlDecode(s);
                    try
                    {
                        if (!string.IsNullOrEmpty(k))
                        {
                            htmlDocument.LoadHtml(k);

                            int count = htmlDocument.DocumentNode.SelectNodes(".//div[@class='date-outer']").Count();
                            if (count > 0)
                            {
                                foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes(".//div[@class='date-outer']"))
                                {
                                    int count2 = node.SelectNodes(".//div[@class='post-outer']").Count();
                                    if (count2 > 0)
                                    {
                                        var Date = node.Descendants("h2").Select(n => n.InnerText).FirstOrDefault().ToString();
                                        string[] parsedDated = Date.ToString().Split(',');
                                        if (parsedDated.Length > 2)
                                        {
                                            string newday = parsedDated[1].Trim().Split(' ')[1];
                                            string newYear = year.ToString();
                                            string newMonth = i.ToString();
                                            DateTime newdate = new DateTime(year, i, int.Parse(newday));

                                            foreach (HtmlNode cn in node.SelectNodes(".//div[@class='post-outer']"))
                                            {
                                                try
                                                {
                                                    foreach (HtmlNode node4 in cn.SelectNodes(".//div[@class='post hentry']"))
                                                    {
                                                        try
                                                        {
                                                            counte++;

                                                            HtmlNode fourthNode = node4.Descendants("h3").FirstOrDefault();
                                                            string newsTitle = fourthNode.Descendants("a").Select(n => n.InnerText).FirstOrDefault().ToString();
                                                            var newsLink = fourthNode.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                                                            string author = "";
                                                            try
                                                            {
                                                                author = node4.Descendants("b").Select(n => n.InnerText).FirstOrDefault().ToString();
                                                            }
                                                            catch
                                                            {
                                                                author = "";
                                                            }
                                                            string publisher = "";
                                                            if (Regex.IsMatch(author, "\\bby\\b"))
                                                            {
                                                                string[] splitted = author.Split('\n');
                                                                if (splitted.Count() < 3)
                                                                {

                                                                }
                                                                author = splitted[0];
                                                                author = Regex.Replace(author, @"\b\s*by\s*\b", "", RegexOptions.IgnoreCase);
                                                                if (splitted.Count() == 3)
                                                                {
                                                                    publisher = splitted[2];
                                                                }
                                                                else if (splitted.Count() == 2)
                                                                    publisher = splitted[1];
                                                                else if (splitted.Count() == 1)
                                                                    publisher = "";
                                                            }

                                                            var articleText = node4.SelectNodes(".//div[@class='post-body entry-content']").Select(n => n.InnerText).FirstOrDefault().ToString();
                                                            DatabaseManager.AddGreeceNews(new Tuple<string, string, string, string, string, string>(newdate.ToShortDateString(), newsTitle.ToString(), newsLink.ToString(), author.ToString(), articleText.ToString(), publisher));
                                                            news.Add(new Tuple<string, string, string, string, string, string>(newdate.ToShortDateString(), newsTitle.ToString(), newsLink.ToString(), author.ToString(), articleText.ToString(), publisher));

                                                        }
                                                        catch (Exception e)
                                                        {

                                                        }

                                                    }
                                                }
                                                catch (Exception e)
                                                {

                                                }


                                            }
                                        }
                                    }



                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        counter++;
                    }

                }
            }

            int x = counter;
            return news;
        }

        public List<Tuple<string, string, string, string, string, string>> FetchGreecNews2()
        {
            List<Tuple<string, string, string, string, string, string>> news = new List<Tuple<string, string, string, string, string, string>>();
            int year = 2015;
            string previusMonth = "11";
            int counter = 0;
            for (int i =1; i < 1686; i++)
            {
                try
                {

                    string display = 0.ToString("00");
                    string url = @"http://english.capital.gr/news_ctg.asp?catid=0&subcat=0&spcatid=0&djcatid=&ps=21&pg=" + i;

                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.OptionDefaultStreamEncoding = Encoding.UTF8;
                    htmlDocument.OptionReadEncoding = true;
                    string s = FetchWebsiteContent(url);
                    string k = System.Net.WebUtility.HtmlDecode(s);

                    if (!string.IsNullOrEmpty(k))
                    {
                        htmlDocument.LoadHtml(k);

                        int count = htmlDocument.DocumentNode.SelectNodes(".//table[@class='bigArticleList']").Count();
                        if (count > 0)
                        {
                            foreach (HtmlNode node in htmlDocument.DocumentNode.SelectNodes(".//tr[@class='alternatingItemStyle']"))
                            {
                                int count2 = node.SelectNodes(".//td[@class='newsItem']").Count();
                                if (count2 > 0)
                                {
                                    var Date = node.Descendants("td").Select(n => n.InnerText).FirstOrDefault().ToString();
                                    string[] parsedDated = Date.ToString().Split('/');
                                    if (parsedDated[1].Trim() == "12" && previusMonth.Trim() == "01")
                                        year = year - 1;

                                    previusMonth = parsedDated[1].Trim();
                                    DateTime newdate = new DateTime(year, int.Parse(parsedDated[1].ToString()), int.Parse(parsedDated[0].ToString()));
                                    foreach (HtmlNode cn in node.SelectNodes(".//td[@class='newsItem']"))
                                    {
                                        List<HtmlNode> fourthNode = cn.Descendants("span").ToList<HtmlNode>();
                                        string newsTitle = fourthNode[0].Descendants("a").Select(n => n.InnerText).FirstOrDefault().ToString();
                                        var newsLink = "http://english.capital.gr" + fourthNode[0].Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                                        string summary = fourthNode[1].InnerText;
                                        summary = summary.Replace("\"", " ");
                                        DatabaseManager.AddGreeceNews(new Tuple<string, string, string, string, string, string>(newdate.ToShortDateString(), newsTitle.ToString(), newsLink.ToString(), "", summary.ToString(), ""));

                                    }


                                }



                            }
                        }
                    }
                }
                catch
                {
                    counter++;
                }
            }
            int x = counter;
            return news;
        }

        public string FetchGreecNews2Article(string Url)
        {
            int counter = 0;
            string article = "";
            try
            {
                
                string display = 0.ToString("00");
                string url = Url;

                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.OptionDefaultStreamEncoding = Encoding.UTF8;
                htmlDocument.OptionReadEncoding = true;
                string s = FetchWebsiteContent(url);
                string k = System.Net.WebUtility.HtmlDecode(s);

                if (!string.IsNullOrEmpty(k))
                {
                    htmlDocument.LoadHtml(k);

                    var spanNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='textbody']");
                    if (spanNode != null)
                    {
                        
                        htmlDocument.LoadHtml(spanNode.InnerHtml);
                        foreach (var node in htmlDocument.DocumentNode.SelectNodes("//p"))
                        {
                            article = article + node.InnerText;
                        }
                       


                    }
                }
            }
            catch
            {
                counter++;
            }

            int x = counter;
            article=article.Replace(@"\n"," ");
            return article;

        }



        #endregion
    }

}

