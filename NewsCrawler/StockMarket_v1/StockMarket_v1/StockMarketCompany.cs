﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockMarket_v1
{
    public class StockMarketCompany
    {
        #region Varaibles 
        int compreanyID;


        int stockMarketIndexId;


        string companyName;


        string indexSymbol;
        #endregion

        #region Properties
        public string IndexSymbol               
        {
            get { return indexSymbol; }
        }
        public int CompreanyID                  
        {
            get { return compreanyID; }
        }
        public int StockMarketIndexId           
        {
            get { return stockMarketIndexId; }
        }
        public string CompanyName               
        {
            get { return companyName; }
        }
        #endregion

        #region Constructors
        public StockMarketCompany(int compreanyID,int stockMarketIndexId, string companyName,string indexSymbol)
        {
            this.companyName = companyName;
            this.stockMarketIndexId = stockMarketIndexId;
            this.companyName = companyName;
            this.indexSymbol = indexSymbol;
        }
        #endregion


    }
}
