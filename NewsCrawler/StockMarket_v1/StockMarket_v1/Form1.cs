﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace StockMarket_v1
{
    public partial class Form1 : Form
    {
        DatabaseManager dm;
        DataImportManager dmi;
        public Form1()                                                                              
        {
           
            dmi= new DataImportManager();
            //dmi.getGuardainData(DateTime.Now, DateTime.Now);
            dm = new DatabaseManager();
            //dmi.ComputeVOlumePercentages();
            //List<Tuple<string, string, string, string, string, string>> Greecenews = dmi.FetchGreecNews2();
           // DatabaseManager.AddGreeceNews(Greecenews);
            //InitializeComponent();
            //LoadStockMarkets();
            dmi.FetchGreecNews2Article(@"http://english.capital.gr/News.asp?id=2402363");
        }

        private void getNews()
        {
            DateTime date =DateTime.Parse("2008-09-30");
            int counter = 0;
            while (counter <= 720)
            {
                dmi.GetReutorsNews(date, date);
                //if (counter % 10 == 0 && counter >0)
                //{
                //    Random r = new Random();
                //    Thread.Sleep(r.Next(120000, 240000));
                //}
                //if (counter % 360 == 0 && counter > 0)
                //{
                //    Random r = new Random();
                //    Thread.Sleep(r.Next(1800000, 5400000));
                //}
                date = date.AddDays(-1);
                counter++;
            }

        }

        private void addNewIndexToolStripMenuItem_Click(object sender, EventArgs e)                 
        {
            OpenFileDialog importDialog = new OpenFileDialog();
            importDialog.Multiselect = false;
            importDialog.ShowDialog();
            importDialog.Filter = "allfiles|*";

            DataImportManager dim = new DataImportManager();
            string s = importDialog.FileName.ToString();
            dim.ImportCompanies(importDialog.FileName.ToString(),int.Parse(textBox1.Text));



        }


        public void LoadStockMarkets()                                                              
        {
            List<StockMarketIndex> smi = DatabaseManager.GetStockMarketsIndices();
            Indicesddl.DataSource = smi;

        }

        private void Indicesddl_SelectedIndexChanged(object sender, EventArgs e)                    
        {
            StockMarketIndex sm = (StockMarketIndex)Indicesddl.SelectedItem;
        }

        private void importIndexInfoToolStripMenuItem_Click(object sender, EventArgs e)             
        {
            OpenFileDialog importDialog = new OpenFileDialog();
            importDialog.Multiselect = false;
            importDialog.ShowDialog();
            importDialog.Filter = "allfiles|*";
           
            DataImportManager dim = new DataImportManager();
            string s = importDialog.FileName.ToString();
            dim.ImportIndexStockDataFromFile(importDialog.FileName.ToString(), int.Parse(textBox1.Text));

        }

        private void importCompanyInfoToolStripMenuItem_Click(object sender, EventArgs e)           
        {
            DialogResult dr = new DialogResult();
                  OpenFileDialog importDialog = new OpenFileDialog();
            importDialog.Multiselect = false;
            dr=importDialog.ShowDialog();
            importDialog.Filter = "allfiles|*";
            if (dr == DialogResult.OK)
            {
                DataImportManager dim = new DataImportManager();
                string s = importDialog.FileName.ToString();
                dim.ImportCOmpanyStockDataFromFile(importDialog.FileName.ToString(), int.Parse(textBox1.Text));
            }
            
        }


        private void importSymbolsToolStripMenuItem_Click(object sender, EventArgs e)               
        {
             DialogResult dr = new DialogResult();
                  OpenFileDialog importDialog = new OpenFileDialog();
            importDialog.Multiselect = false;
            dr=importDialog.ShowDialog();
            importDialog.Filter = "allfiles|*";
            if (dr == DialogResult.OK)
            {
                DataImportManager dim = new DataImportManager();
                string s = importDialog.FileName.ToString();
                dim.ImportSymbols(importDialog.FileName.ToString());
            }
        
        }


    }
}
