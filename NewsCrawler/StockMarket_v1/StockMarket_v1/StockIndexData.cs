﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockMarket_v1
{
   public  class StockIndexData
    {

        int snapshotID;


        DateTime snapShotDate;


        Decimal open;


        Decimal close;


        Decimal high;


        Decimal low;


        Decimal adjClose;


        Decimal volume;

        public Decimal Volume                       
        {
            get { return volume; }
        }
        public Decimal AdjClose                     
        {
            get { return adjClose; }
        }
        public Decimal Low                          
        {
            get { return low; }
        }
        public Decimal High                         
        {
            get { return high; }
        }
        public Decimal Close                        
        {
            get { return close; }
        }
        public Decimal Open                         
        {
            get { return open; }
        }
        public DateTime SnapShotDate            
        {
            get { return snapShotDate; }
        }
        public int SnapshotID                   
        {
            get { return snapshotID; }
        }
        public StockIndexData(int SnapshotID, DateTime SnapshotDate, Decimal Open, Decimal Close, Decimal High, Decimal Low, Decimal AdjClose, Decimal Volume)
        {
            this.volume = Volume;
            this.snapShotDate = SnapshotDate;
            this.open = Open;
            this.close = Close;
            this.high = High;
            this.low = Low;
            this.adjClose = AdjClose;
            this.snapshotID = SnapshotID;
        }
    }
}
